//
//  Cachable.swift
//  SpaceColor
//
//  Created by Thiago dos Santos on 06/01/23.
//

protocol Cacheable<CacheHashable> {
    
    associatedtype CacheHashable
    static func key(_ : CacheHashable) -> String
}
