//
//  Cache.swift
//  SpaceColor
//
//  Created by Thiago dos Santos on 06/01/23.
//

struct Cache<T: Any> {

    class Wrapper<T: Any>: NSObject {
        let value: T
        init(_ object: T) {
            self.value = object
        }
    }

    private var storage = NSCache<NSString, Wrapper<T>>()
    
    func get(key: String) -> T? {
        return storage.object(forKey: NSString(string: key))?.value
    }
    
    func set(key: String, object: T) {
        return storage.setObject(Wrapper(object), forKey: NSString(string: key))
    }
    
    func load(key: String, instantiate: () -> T) -> T {
        if let instance = get(key: key) {
            return instance
        } else {
            let object = instantiate()
            set(key: key, object: object)
            return object
        }
    }
}
