# SpaceColor

[![CI Status](https://img.shields.io/travis/Thiago Medeiros dos Santos/SpaceColor.svg?style=flat)](https://travis-ci.org/Thiago Medeiros dos Santos/SpaceColor)
[![Version](https://img.shields.io/cocoapods/v/SpaceColor.svg?style=flat)](https://cocoapods.org/pods/SpaceColor)
[![License](https://img.shields.io/cocoapods/l/SpaceColor.svg?style=flat)](https://cocoapods.org/pods/SpaceColor)
[![Platform](https://img.shields.io/cocoapods/p/SpaceColor.svg?style=flat)](https://cocoapods.org/pods/SpaceColor)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

SpaceColor is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'SpaceColor'
```

## Author

Thiago Medeiros dos Santos, thiago.santos.it@gmail.com

## License

SpaceColor is available under the MIT license. See the LICENSE file for more info.
