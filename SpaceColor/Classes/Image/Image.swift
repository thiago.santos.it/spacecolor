//
//  Image.swift
//  SpaceColor
//
//  Created by Thiago dos Santos on 28/12/22.
//

struct Image {
    let format: ImageFormat
    let planar: Planar
    let bits: ImageBits
}
