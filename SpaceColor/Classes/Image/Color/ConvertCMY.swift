//
//  ConvertCMY.swift
//  SpaceColor
//
//  Created by Thiago dos Santos on 05/01/23.
//
import Accelerate

struct ConvertCMY: ColorConversion {
    
    private let deComposerCache: DeComposerCache
    
    init(deComposerCache: DeComposerCache) {
        self.deComposerCache = deComposerCache
    }
    
    func convert(image: Image, using colorSpace: ColorSpace) -> Image? {
        
        var result: Planar? = nil
        
        var (planar, alpha) = AlphaChannel().extract(image: image)
        planar = image.format.colorSpace.bgrToRGBIfNeeded(planar: planar)
        
        switch (colorSpace) {
        case .GS, .AGS, .GSA:
            result = ColorSpace.toMono(planar: planar, bits: image.bits)
        case .RGB, .RGBA, .ARGB:
            result = ColorSpace.toRGB(planar: planar, bits: image.bits)
        case .BGRA, .BGR, .ABGR:
            let planarRGB = ColorSpace.cmyToRGB(planar: planar, bits: image.bits)
            result = InterleavedPixelPattern().reversedColors(planar: planarRGB)
        case .CMY, .CMYA, .ACMY:
            result = planar
        case .CMYK, .CMYKA, .ACMYK:
            result = ConvertCMY.toCMYK(planar: planar, bits: image.bits)
        case .RGGB:
            let decomposer = self.deComposerCache.getBayerDecomposer(width: image.format.width, height: image.format.height)
            let rgb = ConvertCMY.toRGB(planar: planar, bits: image.bits)
            let r = rgb.components2D[0], g = rgb.components2D[1], b = rgb.components2D[2]
            result = InterleavedPixel().toMFSM(decomposer: decomposer, components2D: [g, r, g], rowSize: image.format.width)
        case .BGGR:
            let decomposer = self.deComposerCache.getBayerDecomposer(width: image.format.width, height: image.format.height)
            let rgb = ColorSpacetoRGB(planar: planar, bits: image.bits)
            let r = rgb.components2D[0], g = rgb.components2D[1], b = rgb.components2D[2]
            result = InterleavedPixel().toMFSM(decomposer: decomposer, components2D: [g, b, r], rowSize: image.format.width)
        case .GRBG:
            let decomposer = self.deComposerCache.getBayerDecomposer(width: image.format.width, height: image.format.height)
            let rgb = ColorSpace.toRGB(planar: planar, bits: image.bits)
            let r = rgb.components2D[0], g = rgb.components2D[1], b = rgb.components2D[2]
            result = InterleavedPixel().toMFSM(decomposer: decomposer, components2D: [g, r, b], rowSize: image.format.width)
        case .GBRG:
            let decomposer = self.deComposerCache.getBayerDecomposer(width: image.format.width, height: image.format.height)
            let rgb = ColorSpace.toRGB(planar: planar, bits: image.bits)
            let r = rgb.components2D[0], g = rgb.components2D[1], b = rgb.components2D[2]
            result = InterleavedPixel().toMFSM(decomposer: decomposer, components2D: [g, b, r], rowSize: image.format.width)
        case .CYYM:
            let decomposer = self.deComposerCache.getBayerDecomposer(width: image.format.width, height: image.format.height)
            let c = planar.components2D[0], m = planar.components2D[1], y = planar.components2D[2]
            result = InterleavedPixel().toMFSM(decomposer: decomposer, components2D: [y, c, m], rowSize: image.format.width)
        case .MYYC:
            let decomposer = self.deComposerCache.getBayerDecomposer(width: image.format.width, height: image.format.height)
            let c = planar.components2D[0], m = planar.components2D[1], y = planar.components2D[2]
            result = InterleavedPixel().toMFSM(decomposer: decomposer, components2D: [y, m, c], rowSize: image.format.width)
        case .YCMY:
            let decomposer = self.deComposerCache.getBayerDecomposer(width: image.format.width, height: image.format.height)
            let c = planar.components2D[0], m = planar.components2D[1], y = planar.components2D[2]
            result = InterleavedPixel().toMFSM(decomposer: decomposer, components2D: [y, c, m], rowSize: image.format.width)
        case .YMCY:
            let decomposer = self.deComposerCache.getBayerDecomposer(width: image.format.width, height: image.format.height)
            let c = planar.components2D[0], m = planar.components2D[1], y = planar.components2D[2]
            result = InterleavedPixel().toMFSM(decomposer: decomposer, components2D: [y, m, c], rowSize: image.format.width)
        }
        
        result = AlphaChannel().addTo(planar: planar, using: colorSpace, alpha: alpha, bits: image.bits)
        guard let result = result else { return nil }
        return Image(format: image.format.with(colorSpace: colorSpace), planar: result, bits: image.bits)
    }
    
    
    static func toMono(planar: Planar, bits: ImageBits) -> Planar {
        return ColorSpace.rgbToMono(planar: cmyToRGB(planar: planar, bits: bits))
    }
    
    static func toRGB(planar: Planar, bits: ImageBits) -> Planar {
        let r = vDSP.multiply(-bits.max(), vDSP.add(-1, planar.components1D[0]))
        let g = vDSP.multiply(-bits.max(), vDSP.add(-1, planar.components1D[1]))
        let b = vDSP.multiply(-bits.max(), vDSP.add(-1, planar.components1D[2]))
        
        return  Planar(components1D: [r, g, b], rowSize: planar.rowSize)
    }
    
    static func toCMYK(planar: Planar, bits: ImageBits) -> Planar {
      /*  let black = 1.0 - max(cyan, max(magenta, yellow))
          let k = black == 1.0 ? 1.0 : (1.0 - black)/(1.0 - black)
          let c = (cyan - black) * k
          let m = (magenta - black) * k
          let y = (yellow - black) * k
          return (c, m, y, black)
        */
        //TODO:
        //Black (K) = minimum of C,M,Y
        //CyanCMYK = (C - K)/(1 - K)
        //MagentaCMYK = (M - K)/(1 - K)
        //YellowCMYK = (Y - K)/(1 - K)
        return planar
    }
}
