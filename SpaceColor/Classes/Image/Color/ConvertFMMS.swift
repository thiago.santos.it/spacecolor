//
//  ConvertFMMS.swift
//  SpaceColor
//
//  Created by Thiago dos Santos on 05/01/23.
//

struct ConvertFMMS: ColorConversion {
    
    private let deComposerCache: DeComposerCache
    private let debayerCache: DebayerCache
    
    init(deComposerCache: DeComposerCache, debayerCache: DebayerCache) {
        self.deComposerCache = deComposerCache
        self.debayerCache = debayerCache
    }
    
    func convert(image: Image, using colorSpace: ColorSpace) -> Image? {
        return nil
    }
}
