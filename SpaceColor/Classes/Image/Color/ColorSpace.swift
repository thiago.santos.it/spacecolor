//
//  ColorSpace.swift
//  SpaceColor
//
//  Created by Thiago dos Santos on 23/12/22.
//

enum ColorSpace {

    case GS
    case AGS
    case GSA
    case RGB
    case BGR
    case CMY
    case ACMY
    case CMYA
    case ARGB
    case RGBA
    case ABGR
    case BGRA
    case CMYK
    case CMYKA
    case ACMYK
    
    case RGGB
    case BGGR
    case CYYM
    case MYYC
    
    case GRBG
    case GBRG
    case YCMY
    case YMCY
    
    var components: Int {
        get {
            switch (self) {
            case .GS:
                return 1
            case .GSA, .AGS:
                return 2
            case .RGB, .BGR, .CMY:
                return 3
            case .ARGB, .RGBA, .ABGR, .BGRA, .CMYK, .ACMY, .CMYA:
                return 4
            case .CMYKA, .ACMYK:
                return 5
            case .RGGB, .BGGR, .CYYM, .MYYC, .GRBG, .GBRG, .YCMY, .YMCY:
                return 3
            }
        }
    }
    
    var pixelSize: Int {
        get {
            switch (self) {
            case .GS, .GSA, .AGS, .RGB, .BGR, .CMY, .ARGB, .RGBA, .ABGR, .BGRA, .CMYK, .CMYKA, .ACMYK, .ACMY, .CMYA:
                return self.components
            case .RGGB, .BGGR, .CYYM, .MYYC, .GRBG, .GBRG, .YCMY, .YMCY:
                return 1
            }
        }
    }
    
    var bayer: Bool {
        get {
            switch (self) {
            case .GS, .GSA, .AGS, .RGB, .BGR, .CMY, .ARGB, .RGBA, .ABGR, .BGRA, .CMYK, .CMYKA, .ACMYK, .ACMY, .CMYA:
                return false
            case .RGGB, .BGGR, .CYYM, .MYYC, .GRBG, .GBRG, .YCMY, .YMCY:
                return true
            }
        }
    }
    
    var interleaved: Bool {
        get {
            switch (self) {
            case .GS, .GSA, .AGS, .RGB, .BGR, .CMY, .ARGB, .RGBA, .ABGR, .BGRA, .CMYK, .CMYKA, .ACMYK, .ACMY, .CMYA:
                return true
            case .RGGB, .BGGR, .CYYM, .MYYC, .GRBG, .GBRG, .YCMY, .YMCY:
                return false
            }
        }
    }
    
    var hasAlpha: Bool {
        return hasAlphaAtBegging || hasAlphaAtEnd
    }
    
    var hasAlphaAtBegging: Bool {
        switch (self) {
        case .GS, .GSA, .RGB, .BGR, .CMY, .CMYK, .RGGB, .BGGR, .CYYM, .MYYC, .GRBG, .GBRG, .YCMY, .YMCY, .RGBA, .BGRA, .CMYKA, .CMYA:
            return false
        case .AGS, .ARGB, .ABGR, .ACMYK, .ACMY:
            return true
        }
    }
        
    var hasAlphaAtEnd: Bool {
        switch (self) {
        case .GS, .AGS, .RGB, .BGR, .CMY, .CMYK, .RGGB, .BGGR, .CYYM, .MYYC, .GRBG, .GBRG, .YCMY, .YMCY, .ARGB, .ABGR, .ACMYK, .ACMY:
            return false
        case .GSA, .RGBA, .BGRA, .CMYKA, .CMYA:
            return true
        }
    }
    
    var bayerPattern: BayerPattern? {
        switch (self) {
        case  .RGGB, .BGGR, .CYYM, .MYYC:
            return .FirstMainMainSecond
        case  .GRBG, .GBRG, .YCMY, .YMCY:
            return .MainFirstSecondMain
        case .GS, .GSA, .AGS, .RGB, .BGR, .CMY, .ARGB, .RGBA, .ABGR, .BGRA, .CMYK, .CMYKA, .ACMYK, .ACMY, .CMYA:
            return nil
        }
    }
}

