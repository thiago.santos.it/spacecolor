//
//  AlphaChannel.swift
//  SpaceColor
//
//  Created by Thiago dos Santos on 28/12/22.
//

struct AlphaChannel {
    
    func extract(image: Image) -> (Planar, VectorComponent?) {
        let colorSpace = image.format.colorSpace
        var components1D = image.planar.components1D

        if (!colorSpace.hasAlpha) {
            return (image.planar, nil)
        } else if (colorSpace.hasAlphaAtBegging) {
            let alpha1D = components1D[0]
            components1D.removeFirst()
            return (Planar(components1D: components1D, rowSize: image.planar.rowSize), alpha1D)
        } else {
            let alpha1D = components1D[components1D.count - 1]
            components1D.removeLast()
            return (Planar(components1D: components1D, rowSize: image.planar.rowSize), alpha1D)
        }
    }
    
    func addTo(planar: Planar, using colorSpace: ColorSpace, alpha: VectorComponent?, bits: ImageBits) -> Planar {
        let safeAlpha = alpha ?? [Float](repeating: bits.max(), count: planar.components1D.count)
        if (colorSpace.hasAlphaAtBegging) {
            var components1D = planar.components1D
            components1D.insert(safeAlpha, at: 0)
            return Planar(components1D: components1D, rowSize: planar.rowSize)
        } else if (colorSpace.hasAlphaAtEnd) {
            var components1D = planar.components1D
            components1D.append(safeAlpha)
            return Planar(components1D: components1D, rowSize: planar.rowSize)
        } else {
            return planar
        }
    }
}
