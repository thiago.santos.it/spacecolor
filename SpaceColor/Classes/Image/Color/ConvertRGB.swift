//
//  ConvertRGB.swift
//  SpaceColor
//
//  Created by Thiago dos Santos on 05/01/23.
//
import Accelerate

struct ConvertRGB: ColorConversion {
    
    private let deComposerCache: DeComposerCache
    
    init(deComposerCache: DeComposerCache) {
        self.deComposerCache = deComposerCache
    }
    
    func convert(image: Image, using colorSpace: ColorSpace) -> Image? {
        
        var result: Planar? = nil
        
        var (planar, alpha) = AlphaChannel().extract(image: image)
        
        switch (colorSpace) {
        case .GS, .AGS, .GSA:
            result = ConvertRGB.toMono(planar: planar)
        case .RGB, .RGBA, .ARGB:
            result = planar
        case .BGRA, .BGR, .ABGR:
            result = ConvertRGB.toBGR(planar: planar)
        case .CMY, .CMYA, .ACMY:
            result = ConvertRGB.toCMY(planar: planar, bits: image.bits)
        case .CMYK, .CMYKA, .ACMYK:
            result = ConvertRGB.toCMYK(planar: planar, bits: image.bits)
        case .RGGB: 
            let decomposer = self.deComposerCache.getBayerDecomposer(width: image.format.width, height: image.format.height)
            let r = planar.components2D[0], g = planar.components2D[1], b = planar.components2D[2]
            result = InterleavedPixel().toFMMS(decomposer: decomposer, components2D: [g, r, g], rowSize: image.format.width)
        case .BGGR:
            let decomposer = self.deComposerCache.getBayerDecomposer(width: image.format.width, height: image.format.height)
            let r = planar.components2D[0], g = planar.components2D[1], b = planar.components2D[2]
            result = InterleavedPixel().toFMMS(decomposer: decomposer, components2D: [g, b, r], rowSize: image.format.width)
        case .GRBG:
            let decomposer = self.deComposerCache.getBayerDecomposer(width: image.format.width, height: image.format.height)
            let r = planar.components2D[0], g = planar.components2D[1], b = planar.components2D[2]
            result = InterleavedPixel().toMFSM(decomposer: decomposer, components2D: [g, r, b], rowSize: image.format.width)
        case .GBRG:
            let decomposer = self.deComposerCache.getBayerDecomposer(width: image.format.width, height: image.format.height)
            let r = planar.components2D[0], g = planar.components2D[1], b = planar.components2D[2]
            result = InterleavedPixel().toMFSM(decomposer: decomposer, components2D: [g, b, r], rowSize: image.format.width)
        case .CYYM:
            let decomposer = self.deComposerCache.getBayerDecomposer(width: image.format.width, height: image.format.height)
            let cmy = ConvertRGB.toCMY(planar: planar, bits: image.bits)
            let c = planar.components2D[0], m = planar.components2D[1], y = planar.components2D[2]
            result = InterleavedPixel().toFMMS(decomposer: decomposer, components2D: [y, c, m], rowSize: image.format.width)
        case .MYYC:
            let decomposer = self.deComposerCache.getBayerDecomposer(width: image.format.width, height: image.format.height)
            let cmy = ConvertRGB.toCMY(planar: planar, bits: image.bits)
            let c = planar.components2D[0], m = planar.components2D[1], y = planar.components2D[2]
            result = InterleavedPixel().toFMMS(decomposer: decomposer, components2D: [y, m, c], rowSize: image.format.width)
        case .YCMY:
            let decomposer = self.deComposerCache.getBayerDecomposer(width: image.format.width, height: image.format.height)
            let cmy = ConvertRGB.toCMY(planar: planar, bits: image.bits)
            let c = planar.components2D[0], m = planar.components2D[1], y = planar.components2D[2]
            result = InterleavedPixel().toMFSM(decomposer: decomposer, components2D: [y, c, m], rowSize: image.format.width)
        case .YMCY:
            let decomposer = self.deComposerCache.getBayerDecomposer(width: image.format.width, height: image.format.height)
            let cmy = ConvertRGB.toCMY(planar: planar, bits: image.bits)
            let c = planar.components2D[0], m = planar.components2D[1], y = planar.components2D[2]
            result = InterleavedPixel().toMFSM(decomposer: decomposer, components2D: [y, m, c], rowSize: image.format.width)
        }
        
        result = AlphaChannel().addTo(planar: planar, using: colorSpace, alpha: alpha, bits: image.bits)
        guard let result = result else { return nil }
        return Image(format: image.format.with(colorSpace: colorSpace), planar: result, bits: image.bits)
    }
    
    static func toBGR(planar: Planar) -> Planar {
        return Planar(components1D: planar.components1D.reversed().map { $0 }, rowSize: planar.rowSize)
    }
    
    static func toMono(planar: Planar) -> Planar {
        let r = vDSP.multiply(0.299, planar.components1D[0])
        let g = vDSP.multiply(0.587, planar.components1D[1])
        let b = vDSP.multiply(0.114, planar.components1D[2])
        let gray = vDSP.add(r, vDSP.add(g, b))
        return Planar(components1D: [gray], rowSize: planar.rowSize)
    }
    
    static func toCMY(planar: Planar, bits: ImageBits) -> Planar {
        
        let c = vDSP.add(1, vDSP.multiply(Float(-1/bits.max()), planar.components1D[0]))
        let m = vDSP.add(1, vDSP.multiply(Float(-1/bits.max()), planar.components1D[1]))
        let y = vDSP.add(1, vDSP.multiply(Float(-1/bits.max()), planar.components1D[2]))
        
        return Planar(components1D: [c, m, y], rowSize: planar.rowSize)
    }
    
    static func toCMYK(planar: Planar, bits: ImageBits) -> Planar {

        //m stands for "-"
        let mRc = vDSP.multiply(Float(-1/bits.max()), planar.components1D[0]) // -rc
        let mGc = vDSP.multiply(Float(-1/bits.max()), planar.components1D[1]) // -gc
        let mBc = vDSP.multiply(Float(-1/bits.max()), planar.components1D[2]) // -bc
        
        //K = 1 - max(Rc, Gc, Bc) => -K = (1 + min(-Rc, -Gc, -Bc) * -1)
        let k = vDSP.add(1, vDSP.minimum(mRc, vDSP.minimum(mGc, mBc)))
        let mK = vDSP.multiply(-1, k)
        
        //(C,M,Y) = (1 - (Rc,Gc,Bc) - K) ÷ (1 - K) => (C,M,Y) = (1 + (-(Rc,Gc,Bc)) + (-K)) ÷ (1 + (-K))
        let c = vDSP.divide(vDSP.add(1, vDSP.add(mRc, mK)), vDSP.add(1, mK))
        let m = vDSP.divide(vDSP.add(1, vDSP.add(mGc, mK)), vDSP.add(1, mK))
        let y = vDSP.divide(vDSP.add(1, vDSP.add(mBc, mK)), vDSP.add(1, mK))
        
        return Planar(components1D: [c, m, y, k], rowSize: planar.rowSize)
    }
}
