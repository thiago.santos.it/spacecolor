//
//  ConvertInterleaved.swift
//  SpaceColor
//
//  Created by Thiago dos Santos on 17/02/23.
//

import Foundation


struct InterleavedPixel {
    
    func toMFSM(decomposer: BayerDecomposer, components2D: [MatrixComponent], rowSize: Int) -> Planar {
        
        let bayerM = decomposer.toMainFrom(decomposable: Bayer(data: components2D[0], pattern: .MainFirstSecondMain, rowSize: rowSize))
        let bayerF = decomposer.toFirstFrom(decomposable: Bayer(data: components2D[1], pattern: .MainFirstSecondMain, rowSize: rowSize))
        let bayerS = decomposer.toSecondFrom(decomposable: Bayer(data: components2D[2], pattern: .MainFirstSecondMain, rowSize: rowSize))
        
        return Planar(components2D: [bayerM, bayerF, bayerS], rowSize: rowSize)
    }
    
    
    func toFMMS(decomposer: BayerDecomposer, components2D: [MatrixComponent], rowSize: Int) -> Planar {
        let bayerM = decomposer.toMainFrom(decomposable: Bayer(data: components2D[0], pattern: .FirstMainMainSecond, rowSize: rowSize))
        let bayerF = decomposer.toFirstFrom(decomposable: Bayer(data: components2D[1], pattern: .FirstMainMainSecond, rowSize: rowSize))
        let bayerS = decomposer.toSecondFrom(decomposable: Bayer(data: components2D[2], pattern: .FirstMainMainSecond, rowSize: rowSize))
        
        return Planar(components2D: [bayerM, bayerF, bayerS], rowSize: rowSize)
    }
}
