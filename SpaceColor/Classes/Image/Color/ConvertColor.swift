//
//  ConvertColor.swift
//  SpaceColor
//
//  Created by Thiago dos Santos on 05/01/23.
//

protocol ColorConversion {
    func convert(image: Image, using colorSpace: ColorSpace) -> Image?
}

struct ConvertColor {
    
    private let deComposerCache: DeComposerCache
    private let debayerCache: DebayerCache
    
    init(deComposerCache: DeComposerCache, debayerCache: DebayerCache) {
        self.deComposerCache = deComposerCache
        self.debayerCache = debayerCache
    }
    
    func transform(image: Image, to colorSpace: ColorSpace) -> Image? {
        switch (image.format.colorSpace) {
        case .GS, .AGS, .GSA:
            return ConvertMono(deComposerCache: self.deComposerCache).convert(image: image, using: colorSpace)
        case .RGB, .ARGB, .RGBA:
            return ConvertRGB(deComposerCache: self.deComposerCache).convert(image: image, using: colorSpace)
        case .BGR, .ABGR, .BGRA:
            return ConvertBGR(deComposerCache: self.deComposerCache).convert(image: image, using: colorSpace)
        case .CMY, .ACMY, .CMYA:
            return ConvertCMY(deComposerCache: self.deComposerCache).convert(image: image, using: colorSpace)
        case .CMYK, .CMYKA, .ACMYK:
            return ConvertCMYK(deComposerCache: self.deComposerCache).convert(image: image, using: colorSpace)
        case .RGGB, .BGGR, .CYYM, .MYYC:
            return ConvertMFSM(deComposerCache: self.deComposerCache, debayerCache: self.debayerCache).convert(image: image, using: colorSpace)
        case .GRBG, .GBRG, .YCMY, .YMCY:
            return ConvertFMMS(deComposerCache: self.deComposerCache, debayerCache: self.debayerCache).convert(image: image, using: colorSpace)
        }
    }
}
