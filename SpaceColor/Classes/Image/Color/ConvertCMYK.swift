//
//  ConvertCMYK.swift
//  SpaceColor
//
//  Created by Thiago dos Santos on 05/01/23.
//

struct ConvertCMYK: ColorConversion {
    
    private let deComposerCache: DeComposerCache
    
    init(deComposerCache: DeComposerCache) {
        self.deComposerCache = deComposerCache
    }
    
    func convert(image: Image, using colorSpace: ColorSpace) -> Image? {
        
        var result: Planar? = nil
        
        var (planar, alpha) = AlphaChannel().extract(image: image)
        
        planar = image.format.colorSpace.bgrToRGBIfNeeded(planar: planar)
        
        switch (colorSpace) {
        case .GS, .AGS, .GSA:
            result = ColorSpace.cmykToMono(planar: planar, bits: image.bits)
        case .RGB, .RGBA, .ARGB:
            result = ColorSpace.cmykToRGB(planar: planar, bits: image.bits)
        case .BGRA, .BGR, .ABGR:
            let planarRGB = ColorSpace.cmykToRGB(planar: planar, bits: image.bits)
            result = InterleavedPixelPattern().reversedColors(planar: planarRGB)
        case .CMY, .CMYA, .ACMY:
            result = ColorSpace.cmykToCMY(planar: planar, bits: image.bits)
        case .CMYK, .CMYKA, .ACMYK:
            result = planar
        case .RGGB:
            let decomposer = self.deComposerCache.getBayerDecomposer(width: image.format.width, height: image.format.height)
            let rgb = ColorSpace.cmykToRGB(planar: planar, bits: image.bits)
            let r = rgb.components2D[0], g = rgb.components2D[1], b = rgb.components2D[2]
            result = ColorSpace.toMFSM(decomposer: decomposer, components2D: [g, r, g], rowSize: image.format.width)
        case .BGGR:
            let decomposer = self.deComposerCache.getBayerDecomposer(width: image.format.width, height: image.format.height)
            let rgb = ColorSpace.cmykToRGB(planar: planar, bits: image.bits)
            let r = rgb.components2D[0], g = rgb.components2D[1], b = rgb.components2D[2]
            result = ColorSpace.toMFSM(decomposer: decomposer, components2D: [g, b, r], rowSize: image.format.width)
        case .GRBG:
            let decomposer = self.deComposerCache.getBayerDecomposer(width: image.format.width, height: image.format.height)
            let rgb = ColorSpace.cmykToRGB(planar: planar, bits: image.bits)
            let r = rgb.components2D[0], g = rgb.components2D[1], b = rgb.components2D[2]
            result = ColorSpace.toMFSM(decomposer: decomposer, components2D: [g, r, b], rowSize: image.format.width)
        case .GBRG:
            let decomposer = self.deComposerCache.getBayerDecomposer(width: image.format.width, height: image.format.height)
            let rgb = ColorSpace.cmykToRGB(planar: planar, bits: image.bits)
            let r = rgb.components2D[0], g = rgb.components2D[1], b = rgb.components2D[2]
            result = ColorSpace.toMFSM(decomposer: decomposer, components2D: [g, b, r], rowSize: image.format.width)
        case .CYYM:
            let decomposer = self.deComposerCache.getBayerDecomposer(width: image.format.width, height: image.format.height)
            let cmy = ColorSpace.cmykToCMY(planar: planar, bits: image.bits)
            let c = cmy.components2D[0], m = cmy.components2D[1], y = cmy.components2D[2]
            result = ColorSpace.toMFSM(decomposer: decomposer, components2D: [y, c, m], rowSize: image.format.width)
        case .MYYC:
            let decomposer = self.deComposerCache.getBayerDecomposer(width: image.format.width, height: image.format.height)
            let cmy = ColorSpace.cmykToCMY(planar: planar, bits: image.bits)
            let c = cmy.components2D[0], m = cmy.components2D[1], y = cmy.components2D[2]
            result = ColorSpace.toMFSM(decomposer: decomposer, components2D: [y, m, c], rowSize: image.format.width)
        case .YCMY:
            let decomposer = self.deComposerCache.getBayerDecomposer(width: image.format.width, height: image.format.height)
            let cmy = ColorSpace.cmykToCMY(planar: planar, bits: image.bits)
            let c = cmy.components2D[0], m = cmy.components2D[1], y = cmy.components2D[2]
            result = ColorSpace.toMFSM(decomposer: decomposer, components2D: [y, c, m], rowSize: image.format.width)
        case .YMCY:
            let decomposer = self.deComposerCache.getBayerDecomposer(width: image.format.width, height: image.format.height)
            let cmy = ColorSpace.cmykToCMY(planar: planar, bits: image.bits)
            let c = cmy.components2D[0], m = cmy.components2D[1], y = cmy.components2D[2]
            result = ColorSpace.toMFSM(decomposer: decomposer, components2D: [y, m, c], rowSize: image.format.width)
        }
        
        result = AlphaChannel().addTo(planar: planar, using: colorSpace, alpha: alpha, bits: image.bits)
        guard let result = result else { return nil }
        return Image(format: image.format.with(colorSpace: colorSpace), planar: result, bits: image.bits)
    }
    
    
    static func toMono(planar: Planar, bits: ImageBits) -> Planar {
        return ColorSpace.rgbToMono(planar: cmykToRGB(planar: planar, bits: bits))
    }
    
    static func toRGB(planar: Planar, bits: ImageBits) -> Planar {
        
        //Red = 255 × ( 1 - Cyan ÷ 100 ) × ( 1 - Black ÷ 100 )
        //Green = 255 × ( 1 - Magenta ÷ 100 ) × ( 1 - Black ÷ 100 )
        //Blue = 255 × ( 1 - Yellow ÷ 100 ) × ( 1 - Black ÷ 100 )
        let max = bits.max()
        
        let c = vDSP.add(1, vDSP.multiply(Float(-0.01), planar.components1D[0]))
        let m = vDSP.add(1, vDSP.multiply(Float(-0.01), planar.components1D[1]))
        let y = vDSP.add(1, vDSP.multiply(Float(-0.01), planar.components1D[2]))
        let k = vDSP.add(1, vDSP.multiply(Float(-0.01), planar.components1D[3]))
        
        let r = vDSP.multiply(max, vDSP.multiply(c, k))
        let g = vDSP.multiply(max, vDSP.multiply(m, k))
        let b = vDSP.multiply(max, vDSP.multiply(y, k))
        
        return Planar(components1D: [r, g, b], rowSize: planar.rowSize)
    }
    
    static func toCMY(planar: Planar, bits: ImageBits) -> Planar {
     /*   let c = (cyan * (1.0 - key) + key)
            let m = (magenta * (1.0 - key) + key)
            let y = (yellow * (1.0 - key) + key)
            return (c, m, y)*/
        return planar
    }
    
}
