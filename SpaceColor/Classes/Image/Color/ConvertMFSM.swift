//
//  ConvertMFSM.swift
//  SpaceColor
//
//  Created by Thiago dos Santos on 05/01/23.
//

struct ConvertMFSM: ColorConversion {
    
    private let deComposerCache: DeComposerCache
    private let debayerCache: DebayerCache
    
    init(deComposerCache: DeComposerCache, debayerCache: DebayerCache) {
        self.deComposerCache = deComposerCache
        self.debayerCache = debayerCache
    }
    
    func convert(image: Image, using colorSpace: ColorSpace) -> Image? {
        
        var result: Planar? = nil
        
        var (planar, alpha) = AlphaChannel().extract(image: image)
        planar = image.format.colorSpace.bgrToRGBIfNeeded(planar: planar)
        
        switch (colorSpace) {
        case .GS, .AGS, .GSA:
            result = ColorSpace.rgbToMono(planar: planar)
        case .RGB, .RGBA, .ARGB:
            result = planar
        case .BGRA, .BGR, .ABGR:
            result = InterleavedPixelPattern().reversedColors(planar: planar)
        case .CMY, .CMYA, .ACMY:
            result = ColorSpace.rgbToCMY(planar: planar, bits: image.bits)
        case .CMYK, .CMYKA, .ACMYK:
            result = ColorSpace.rgbToCMYK(planar: planar, bits: image.bits)
        case .RGGB:
            let decomposer = self.deComposerCache.getBayerDecomposer(width: image.format.width, height: image.format.height)
            let r = planar.components2D[0], g = planar.components2D[1], b = planar.components2D[2]
            result = ColorSpace.toFMMS(decomposer: decomposer, components2D: [g, r, g], rowSize: image.format.width)
        case .BGGR:
            let decomposer = self.deComposerCache.getBayerDecomposer(width: image.format.width, height: image.format.height)
            let r = planar.components2D[0], g = planar.components2D[1], b = planar.components2D[2]
            result = ColorSpace.toFMMS(decomposer: decomposer, components2D: [g, b, r], rowSize: image.format.width)
        case .GRBG:
            let decomposer = self.deComposerCache.getBayerDecomposer(width: image.format.width, height: image.format.height)
            let r = planar.components2D[0], g = planar.components2D[1], b = planar.components2D[2]
            result = ColorSpace.toMFSM(decomposer: decomposer, components2D: [g, r, b], rowSize: image.format.width)
        case .GBRG:
            let decomposer = self.deComposerCache.getBayerDecomposer(width: image.format.width, height: image.format.height)
            let r = planar.components2D[0], g = planar.components2D[1], b = planar.components2D[2]
            result = ColorSpace.toMFSM(decomposer: decomposer, components2D: [g, b, r], rowSize: image.format.width)
        case .CYYM:
            let decomposer = self.deComposerCache.getBayerDecomposer(width: image.format.width, height: image.format.height)
            let cmy = ColorSpace.rgbToCMY(planar: planar, bits: image.bits)
            let c = planar.components2D[0], m = planar.components2D[1], y = planar.components2D[2]
            result = ColorSpace.toFMMS(decomposer: decomposer, components2D: [y, c, m], rowSize: image.format.width)
        case .MYYC:
            let decomposer = self.deComposerCache.getBayerDecomposer(width: image.format.width, height: image.format.height)
            let cmy = ColorSpace.rgbToCMY(planar: planar, bits: image.bits)
            let c = planar.components2D[0], m = planar.components2D[1], y = planar.components2D[2]
            result = ColorSpace.toFMMS(decomposer: decomposer, components2D: [y, m, c], rowSize: image.format.width)
        case .YCMY:
            let decomposer = self.deComposerCache.getBayerDecomposer(width: image.format.width, height: image.format.height)
            let cmy = ColorSpace.rgbToCMY(planar: planar, bits: image.bits)
            let c = planar.components2D[0], m = planar.components2D[1], y = planar.components2D[2]
            result = ColorSpace.toMFSM(decomposer: decomposer, components2D: [y, c, m], rowSize: image.format.width)
        case .YMCY:
            let decomposer = self.deComposerCache.getBayerDecomposer(width: image.format.width, height: image.format.height)
            let cmy = ColorSpace.rgbToCMY(planar: planar, bits: image.bits)
            let c = planar.components2D[0], m = planar.components2D[1], y = planar.components2D[2]
            result = ColorSpace.toMFSM(decomposer: decomposer, components2D: [y, m, c], rowSize: image.format.width)
        }
        
        result = AlphaChannel().addTo(planar: planar, using: colorSpace, alpha: alpha, bits: image.bits)
        guard let result = result else { return nil }
        return Image(format: image.format.with(colorSpace: colorSpace), planar: result, bits: image.bits)
    }
}
