//
//  ConvertMono.swift
//  SpaceColor
//
//  Created by Thiago dos Santos on 05/01/23.
//

import Accelerate

struct ConvertMono: ColorConversion {
    
    private let deComposerCache: DeComposerCache
    
    init(deComposerCache: DeComposerCache) {
        self.deComposerCache = deComposerCache
    }
    
    func convert(image: Image, using colorSpace: ColorSpace) -> Image? {
        
        var result: Planar? = nil
        
        let (planar, alpha) = AlphaChannel().extract(image: image)

        switch (colorSpace) {
        case .GS, .AGS, .GSA:
            result =  planar
        case .RGB, .BGR, .RGBA, .BGRA, .ARGB, .ABGR:
            result = ConvertMono.toRGB(planar: planar)
        case .CMY, .CMYA, .ACMY:
            result = ConvertMono.toCMY(planar: planar, bits: image.bits)
        case .CMYK, .CMYKA, .ACMYK:
            result = ConvertMono.toCMYK(planar: planar, bits: image.bits)
        case .RGGB, .BGGR:
            let decomposer = self.deComposerCache.getBayerDecomposer(width: image.format.width, height: image.format.height)
                                ?? BayerDecomposer(width: image.format.width, height: image.format.height)
            
            let bayer = Bayer(data: planar.components2D[0], pattern: .FirstMainMainSecond, rowSize: image.format.width)
            
            result = decomposer.toPlanarFrom(decomposable: bayer)
        case .GRBG, .GBRG:
            let decomposer = self.deComposerCache.getBayerDecomposer(width: image.format.width, height: image.format.height)
                                ?? BayerDecomposer(width: image.format.width, height: image.format.height)
            
            let bayer = Bayer(data: planar.components2D[0], pattern: .MainFirstSecondMain, rowSize: image.format.width)
            
            result = decomposer.toPlanarFrom(decomposable: bayer)
            
        case .CYYM, .MYYC:
            let decomposer = self.deComposerCache.getBayerDecomposer(width: image.format.width, height: image.format.height)
                                ?? BayerDecomposer(width: image.format.width, height: image.format.height)
            
            let cmy = ConvertMono.toCMY(planar: planar, bits: image.bits)
            
            let bayer = Bayer(data: cmy.components2D[0], pattern: .FirstMainMainSecond, rowSize: image.format.width)
            
            result = decomposer.toPlanarFrom(decomposable: bayer)
            
        case .YCMY, .YMCY:
            let decomposer = self.deComposerCache.getBayerDecomposer(width: image.format.width, height: image.format.height)
                                ?? BayerDecomposer(width: image.format.width, height: image.format.height)
            
            let cmy = ConvertMono.toCMY(planar: planar, bits: image.bits)
           
            let bayer = Bayer(data: cmy.components2D[0], pattern: .MainFirstSecondMain, rowSize: image.format.width)
            
            result = decomposer.toPlanarFrom(decomposable: bayer)
        }
        
        result = AlphaChannel().addTo(planar: planar, using: colorSpace, alpha: alpha, bits: image.bits)
        guard let result = result else { return nil }
        return Image(format: image.format.with(colorSpace: colorSpace), planar: result, bits: image.bits)
    }
    
    static func toRGB(planar: Planar) -> Planar {
        let gray = planar.components1D[0]
        return Planar(components1D: [gray, gray, gray], rowSize: planar.rowSize)
    }
    
    static func toCMY(planar: Planar, bits: ImageBits) -> Planar {
        let gray = vDSP.divide(bits.max()/100, planar.components1D[0])
        return Planar(components1D: [gray, gray, gray], rowSize: planar.rowSize)
    }
    
    static func toCMYK(planar: Planar, bits: ImageBits) -> Planar {
        let gray = vDSP.divide(bits.max()/100, planar.components1D[0])
        let empty = VectorComponent(repeating: 0, count: gray.count)
        return Planar(components1D: [empty, empty, empty, gray], rowSize: planar.rowSize)
    }
}
