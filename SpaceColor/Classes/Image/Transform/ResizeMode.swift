//
//  ResizeMode.swift
//  SpaceColor
//
//  Created by Thiago dos Santos on 05/01/23.
//

struct ResizeMode {
    
    enum Up {
        case AspectFit
        case AspectFill
        case ScaleToFill
    }
    
    enum Down {
        case Crop
        case AspectFit
        case AspectFill
        case ScaleToFill
    }
    
    let up: Up
    let down: Down
}
