//
//  ImageFormat.swift
//  SpaceColor
//
//  Created by Thiago dos Santos on 22/12/22.
//

struct ImageFormat: Hashable {
    
    let width: Int
    let height: Int
    let colorSpace: ColorSpace
    
    var pixelSize: Int { colorSpace.pixelSize }
    
    var components: Int { colorSpace.components }
    
    func rowSize() -> Int {
        return self.width * self.colorSpace.pixelSize
    }
    
    func size() -> Int {
        return self.height * self.width * self.colorSpace.pixelSize
    }
    
    func pixels() -> Int {
        return self.height * self.width
    }
    
    func with(colorSpace: ColorSpace) -> ImageFormat {
        return ImageFormat(width: self.width, height: self.height, colorSpace: colorSpace)
    }
    
    func with(width: Int) -> ImageFormat {
        return ImageFormat(width: width, height: self.height, colorSpace: self.colorSpace)
    }
 
    func with(height: Int) -> ImageFormat {
        return ImageFormat(width: self.width, height: height, colorSpace: self.colorSpace)
    }
}
