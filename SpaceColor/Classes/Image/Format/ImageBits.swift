//
//  ImageBits.swift
//  SpaceColor
//
//  Created by Thiago dos Santos on 29/12/22.
//

enum ImageBits {
    case U16
    case S16
    case U8
    case S8
    
    func max() -> Float {
        switch (self) {
        case .U8:
            return Float(UInt8.max)
        case .S8:
            return Float(Int8.max)
        case .U16:
            return Float(UInt16.max)
        case .S16:
            return Float(Int16.max)
        }
    }
}
