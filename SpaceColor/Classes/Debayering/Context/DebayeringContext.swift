//
//  DebayeringContext.swift
//  SpaceColor
//
//  Created by Thiago dos Santos on 29/12/22.
//


struct DebayeringContext {
    
    let imageFormat: ImageFormat
    let pattern: BayerPattern
    let storage: Storage
    
    init?(imageFormat: ImageFormat) {
        guard let pattern = imageFormat.colorSpace.bayerPattern else {
            print("Ivalid bayer pattern from image format")
            return nil
        }
        self.pattern = pattern
        self.imageFormat = imageFormat
        self.storage = Storage(imageFormat: imageFormat)
    }
        
    struct Storage {
        let emptyRow: [Float]
        let emptyMatrix: [[Float]]

        init(imageFormat: ImageFormat) {
            self.emptyRow = [Float](repeating: 0.0, count: imageFormat.width)
            self.emptyMatrix = [[Float]] (repeating: self.emptyRow, count: imageFormat.height)
        }
    }
    
    static func hashFor(format: ImageFormat) -> String {
        return "DebayeringContext(format:\(format))"
    }
}
