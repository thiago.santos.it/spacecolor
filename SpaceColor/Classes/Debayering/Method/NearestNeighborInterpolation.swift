//
//  NearestNeighborInterpolation.swift
//  SpaceColor
//
//  Created by Thiago dos Santos on 23/12/22.
//

import Accelerate

class NearestNeightborInterpolation: Debayer {
    
    typealias CacheHashable = Image
    
    private let context: DebayeringContext
    
    private var mainWeights: [[Float]] = []
    
    init(context: DebayeringContext, decomposer: BayerDecomposer? = nil) {
        self.context = context
        
        let imageFormat = context.imageFormat
        let decomposer = decomposer ?? BayerDecomposer(width: imageFormat.width, height: imageFormat.height)
        
        let unit = [Float](repeating: 1.0, count: imageFormat.size()).split(by: imageFormat.rowSize())
        let decomposedUnit = decomposer.toPlanarFrom(decomposable: Bayer(data: unit, pattern: context.pattern, rowSize: imageFormat.rowSize()))
        
        self.mainWeights = self.main(decomposedUnit.components2D[0], weights: unit).oneOver()
    }
    
    func apply(planar: Planar) -> Planar {
        let firstMainMainSecond = (self.context.pattern == BayerPattern.FirstMainMainSecond)
        let mainFirstSecondMain = (self.context.pattern == BayerPattern.MainFirstSecondMain)
        
        let main = self.main(planar.components2D[0], weights: mainWeights)
        let first = self.line(planar.components2D[1], from: 0)
        let second = self.line(planar.components2D[2], from: 1)
        return Planar(components2D: [main, first, second], rowSize: planar.rowSize)
    }
    
    private func main(_ matrix: [[Float]], weights: [[Float]]) -> [[Float]] {
        let zeroFirst = (context.pattern == BayerPattern.FirstMainMainSecond)
        let oddWidth = self.context.imageFormat.width % 2 != 0
        
        let height = self.context.imageFormat.height
        var row = self.context.storage.emptyRow
        
        return (0..<height).map { index in
            
            row = matrix[index]
            
            //Add nextline
            if (index + 1 < height) {
                row = vDSP.add(matrix[index], matrix[index + 1])
            }
            
            //Add neighboor at next position
            if (zeroFirst) {
                if (index % 2 == 0) {
                    row = vDSP.add(matrix[index], matrix[index] << 1)
                    if (oddWidth) {
                        row[row.count - 1] = row[row.count - 2]
                    }
                } else {
                    row = vDSP.add(matrix[index], matrix[index] >> 1)
                }
            } else {
                if (index % 2 == 0) {
                    row = vDSP.add(matrix[index], matrix[index] >> 1)
                } else {
                    row = vDSP.add(matrix[index], matrix[index] << 1)
                    if (oddWidth) {
                        row[row.count - 1] = row[row.count - 2]
                    }
                }
            }
            
            vDSP.multiply(row, weights[index], result: &row)
            
            return row
        }
    }
    
    private func line(_ matrix: [[Float]], from startingAt: Int) -> [[Float]] {
        let startWithZero = (self.context.pattern == .MainFirstSecondMain && startingAt == 0) ||
                                (self.context.pattern == .FirstMainMainSecond && startingAt == 1)
        
        let oddWidth = self.context.imageFormat.width % 2 != 0
        let oddHeight = self.context.imageFormat.height % 2 != 0
        
        let height = self.context.imageFormat.height
        var result = self.context.storage.emptyMatrix
       
        stride(from: startingAt, to: height, by: 2).forEach { index in
            var row = matrix[index]
            
            if (startWithZero) {
                vDSP.add(row, row << 1, result: &row)
                if (oddWidth) {
                    row[row.count - 1] = row[row.count - 2]
                }
            } else {
                vDSP.add(row, row >> 1, result: &row)
            }
            
            result[index] = row
            let copyTo = index + (startingAt == 0 ? 1 : -1)
            if (copyTo < height) {
                result[copyTo] = row
            }
        }
        if (oddHeight && startingAt == 1) {
            result[result.count - 1] = result[result.count - 2]
        }
        return result
    }

    static func key(_ image: Image) -> String {
        return "NearestNeightborInterpolation(context:\(DebayeringContext.hashFor(format: image.format))"
    }
}
