//
//  BilinearInterpolation.swift
//  SpaceColor
//
//  Created by Thiago dos Santos on 12/12/22.
//

import Accelerate

class BilinearInterpolation: Debayer {
    
    typealias CacheHashable = Image
    
    struct Storage {
        let weights: [MatrixComponent]
    }
    private var storage: Storage = Storage(weights: [])
    
    private let context: DebayeringContext
    private let queue: DispatchQueue
    private let group: DispatchGroup
    
    init(context: DebayeringContext, decomposer: BayerDecomposer? = nil) {
       
        self.queue = DispatchQueue(label: "BilinearInterpolation.queue.\(DebayeringContext.hashFor(format: context.imageFormat))")
        self.group = DispatchGroup()
        self.context = context
        
        let imageFormat = context.imageFormat
        let decomposer = decomposer ?? BayerDecomposer(width: imageFormat.width, height: imageFormat.height)
        
        let unit = [Float](repeating: 1.0, count: imageFormat.size()).split(by: imageFormat.rowSize())
        
        let decomposedUnit = decomposer.toPlanarFrom(decomposable: Bayer(data: unit, pattern: context.pattern, rowSize: context.imageFormat.rowSize()))
        let components2D = self.interpolation(components2D: decomposedUnit.components2D,
                                              weights: [unit, unit, unit])
        
        self.storage = BilinearInterpolation.Storage(weights: [components2D[0].oneOver(),
                                                           components2D[1].oneOver(),
                                                           components2D[2].oneOver()])
    }
    
    func apply(planar: Planar) -> Planar {
        let components2D = self.interpolation(components2D: planar.components2D, weights: self.storage.weights)
        return Planar(components2D: components2D, rowSize: planar.rowSize)
    }
    
    private func interpolation(components2D: [MatrixComponent], weights: [MatrixComponent]) -> [MatrixComponent] {
        var result = [MatrixComponent](repeating: [], count: 3)
        group.enter()
        queue.async {
            result[0] = self.weight(self.vertical(self.horizontal(components2D[0], from: 0, by: 1)), weight: weights[0])
            self.group.leave()
        }
        group.enter()
        queue.async {
            result[1] = self.weight(self.vertical(self.horizontal(components2D[1], from: 0, by: 2), from: 1), weight: weights[1])
            self.group.leave()
        }
        group.enter()
        queue.async {
            result[2] = self.weight(self.vertical(self.horizontal(components2D[2], from: 1, by: 2), from: 0), weight: weights[2])
            self.group.leave()
        }
        group.wait()
        
        return result
    }
    
    private func horizontal(_ matrix: MatrixComponent, from startingAt: Int = 0, by strideBy: Int = 1) -> MatrixComponent {
        let height = self.context.imageFormat.height
        var result = self.context.storage.emptyMatrix
        
        stride(from: startingAt, to: height, by: strideBy).forEach { index in
            result[index] = vDSP.add(vDSP.add(matrix[index], matrix[index] << 1), matrix[index] >> 1)
        }
        
        return result
    }
    
    private func vertical(_ matrix: MatrixComponent, from startingAt: Int, by strideBy: Int = 2) -> MatrixComponent {
        let height = self.context.imageFormat.height
        var result = matrix
        
        stride(from: startingAt, to: height, by: strideBy).forEach { index in
            var line = matrix[index]
            if (index > 0) {
                line = vDSP.add(line, matrix[index - 1])
            }
            if (index < height - 1) {
                line = vDSP.add(line, matrix[index + 1])
            }
            result[index] = line
        }
        return result
    }
    
    private func vertical(_ matrix: MatrixComponent) -> MatrixComponent {
        let reverseInFirstLine = (self.context.pattern == .MainFirstSecondMain)
        let height = self.context.imageFormat.height
        
        let stride = vDSP_Stride(2)
        let length = vDSP_Length(ceil(Float(matrix[0].count)/2.0))
        
        return (0..<height).map { index in
            let reverse = (reverseInFirstLine && index % 2 == 0) || (!reverseInFirstLine && index % 2 != 0)
            var row = matrix[index]
            if (reverse) { vDSP.reverse(&row) }
            
            if (index > 0) {
                var previousRow  = matrix[index - 1]
                if (reverse) { vDSP.reverse(&previousRow) }
                vDSP_vadd(row, stride, previousRow, stride, &row, stride, length)
            }
            
            if (index < height - 1) {
                var nextRow  = matrix[index + 1]
                if (reverse) { vDSP.reverse(&nextRow) }
                vDSP_vadd(row, stride, nextRow, stride, &row, stride, length)
            }
            
            if (reverse) { vDSP.reverse(&row) }
            return row
        }
    }
    
    private func weight(_ matrix: MatrixComponent, weight: MatrixComponent) -> MatrixComponent {
        return zip(matrix, weight).map { vDSP.multiply($0, $1) }
    }
    
    static func key(_ image: Image) -> String {
        return "BilinearInterpolation(context:\(DebayeringContext.hashFor(format: image.format)))"
    }
}
