//
//  DebayerMethod.swift
//  SpaceColor
//
//  Created by Thiago dos Santos on 09/01/23.
//

enum DebayerMethod {
    case BILINEAR
    case NEAREST_NEIGHBOR
}
