//
//  Debayer.swift
//  SpaceColor
//
//  Created by Thiago dos Santos on 13/12/22.
//

protocol Debayer: Cacheable {    
    func apply(planar: Planar) -> Planar
}

