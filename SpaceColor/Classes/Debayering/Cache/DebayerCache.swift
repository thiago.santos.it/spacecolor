//
//  DebayerCache.swift
//  SpaceColor
//
//  Created by Thiago dos Santos on 09/01/23.
//

import Foundation


class DebayerCache {
    
    let debayerCache: Cache<any Debayer> = Cache()
    
    let method: DebayerMethod
    
    init(method: DebayerMethod = .BILINEAR) {
        self.method = method
    }
    
    func debayerFor(image: Image) -> (any Debayer)? {
        if let context = DebayeringContext(imageFormat: image.format) {
            return debayerCache.get(key: self.keyFor(image: image)) ?? self.instanceFor(image: image)
        }
        return nil
    }
    
    func keyFor(image: Image) -> String {
        switch (method) {
        case .BILINEAR:
            return BilinearInterpolation.key(image)
        case .NEAREST_NEIGHBOR:
            return NearestNeightborInterpolation.key(image)
        }
    }
    
    func instanceFor(image: Image) -> (any Debayer)? {
        if let context = DebayeringContext(imageFormat: image.format) {
            switch (method) {
            case .BILINEAR:
                return BilinearInterpolation(context: context)
            case .NEAREST_NEIGHBOR:
                return NearestNeightborInterpolation(context: context)
            }
        }
        return nil
    }
}
