//
//  Metrics.swift
//  SpaceColor
//
//  Created by Thiago dos Santos on 05/01/23.
//

import Foundation

struct Metrics {
    static func currentTimeInMiliseconds() -> Int {
        let currentDate = Date()
        let since1970 = currentDate.timeIntervalSince1970
        return Int(since1970 * 1000000)
    }
    
    static func run(name: String, block: ()-> Void) {
        let start = currentTimeInMiliseconds()
        block()
        print("\(name): \(currentTimeInMiliseconds() - start)µs")
    }
}
