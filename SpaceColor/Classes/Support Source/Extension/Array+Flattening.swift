//
//  Array+Flattening.swift
//  SpaceColor
//
//  Created by Thiago Santos on 18/12/22.
//  Copyright © 2022 CocoaPods. All rights reserved.
//

extension [[Float]] {
    func flatten() -> [Float] {
        return self.flatMap { $0 }
    }
}
