//
//  Images+Conversion.swift
//  SpaceColor
//
//  Created by <https://gist.github.com/dagronf/6d41c62a098b1e9a2f4397fd85aba207>

import CoreGraphics
import AppKit

extension NSImage {
   func asCIImage() -> CIImage? {
      if let cgImage = self.asCGImage() {
         return CIImage(cgImage: cgImage)
      }
      return nil
   }

    func asCGImage() -> CGImage? {
      var rect = NSRect(origin: CGPoint(x: 0, y: 0), size: self.size)
      return self.cgImage(forProposedRect: &rect, context: NSGraphicsContext.current, hints: nil)
    }
}

extension CIImage {
   
    func asCGImage(context: CIContext? = nil) -> CGImage? {
      let ctx = context ?? CIContext(options: nil)
      return ctx.createCGImage(self, from: self.extent)
   }

    func asNSImage(pixelsSize: CGSize? = nil, repSize: CGSize? = nil) -> NSImage? {
      let rep = NSCIImageRep(ciImage: self)
      if let ps = pixelsSize {
         rep.pixelsWide = Int(ps.width)
         rep.pixelsHigh = Int(ps.height)
      }
      if let rs = repSize {
         rep.size = rs
      }
      let updateImage = NSImage(size: rep.size)
      updateImage.addRepresentation(rep)
      return updateImage
   }
}

extension CGImage {
   
    func asCIImage() -> CIImage? {
      return CIImage(cgImage: self)
   }

    func asNSImage() -> NSImage? {
      return NSImage(cgImage: self, size: .zero)
   }
}
