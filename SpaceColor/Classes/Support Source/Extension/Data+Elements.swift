//
//  Data+Elements.swift
//  SpaceColor
//
//  Created by Thiago dos Santos on 12/12/22.
//

extension Data {
    
    func elements<T: FixedWidthInteger>() -> [T] {
        var result = [T](repeating: 0, count: self.count / MemoryLayout<T>.stride)
        _ = result.withUnsafeMutableBytes { self.copyBytes(to: $0) }
        return result
    }
}
