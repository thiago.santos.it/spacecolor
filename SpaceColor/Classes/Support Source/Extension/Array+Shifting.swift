//
//  Array+Shifting.swift
//  SpaceColor
//
//  Created by Thiago dos Santos on 12/12/22.
//
import Accelerate

extension [Float] {
    
    static func >>(lhs: [Float], rhs: Int) -> [Float] {
        return lhs.shiftRight(by: rhs)
    }
    
    static func <<(lhs: [Float], rhs: Int) -> [Float] {
        return lhs.shiftLeft(by: rhs)
    }

    private func shiftLeft(by times: Int) -> [Float] {
        var mutating = self
        return copyLeft(shifting: times, data: &mutating)
    }
    
    private func shiftRight(by times: Int) -> [Float] {
        var mutating = self
        return copyRight(shifting: times, data: &mutating)
    }
    
    private func copyLeft(shifting shift: Int = 1, data: inout [Float]) -> [Float] {
        let lastIndex = data.count - 1
        var pointer: UnsafePointer<Float> = &data + shift
        var result = data
        cblas_scopy(Int32(data.count - shift), pointer, 1, &result, 1)
        if (shift > 1) {
            (0..<shift).forEach { index in result[lastIndex - index] = 0}
        } else {
            result[lastIndex] = 0
        }
        return result
    }
    
    private func copyRight(shifting shift: Int = 1, data: inout [Float]) -> [Float] {
        var result = data
        var pointer: UnsafeMutablePointer<Float> = &result + shift
        cblas_scopy(Int32(data.count - shift), &data, 1, pointer, 1)
        if (shift > 1) {
            (0..<shift).forEach { index in result[index] = 0}
        } else {
            result[0] = 0
        }
        return result
    }
}
