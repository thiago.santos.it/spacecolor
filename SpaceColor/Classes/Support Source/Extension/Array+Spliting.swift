//
//  Data+Spliting.swift
//  SpaceColor
//
//  Created by Thiago dos Santos on 12/12/22.
//

extension [Float] {
    func split(by size: Int) -> [[Float]] {
        return stride(from: 0, to: count, by: size).map {
            Array(self[$0 ..< Swift.min($0 + size, count)])
        }
    }
}
