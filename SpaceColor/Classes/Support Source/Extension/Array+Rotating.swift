//
//  Array+Rotating.swift
//  SpaceColor
//
//  Created by Thiago Santos on 18/12/22.
//  Copyright © 2022 CocoaPods. All rights reserved.
//
import Accelerate

infix operator +<<
infix operator >>+

extension [Float] {
    
    static func >>+(lhs: [Float], rhs: Int) -> [Float] {
        return lhs.rotateRight(by: rhs)
    }
    
    static func +<<(lhs: [Float], rhs: Int) -> [Float] {
        return lhs.rotateLeft(by: rhs)
    }
    
    private func rotateLeft(by times: Int) -> [Float] {
        var mutating = self
        return copyLeft(shifting: times, data: &mutating)
    }
    
    private func rotateRight(by times: Int) -> [Float] {
        var mutating = self
        return copyRight(shifting: times, data: &mutating)
    }
    
    private func copyLeft(shifting shift: Int = 1, data: inout [Float]) -> [Float] {
        let lastIndex = data.count - 1
        var pointer: UnsafePointer<Float> = &data + shift
        var result = data
        cblas_scopy(Int32(data.count - shift), pointer, 1, &result, 1)
        if (shift > 1) {
            (0..<shift).forEach { index in result[lastIndex - (shift - 1 - index)] = self[index]}
        } else {
            result[lastIndex] = self[0]
        }
        return result
    }
    
    private func copyRight(shifting shift: Int = 1, data: inout [Float]) -> [Float] {
        let lastIndex = data.count - 1
        var result = data
        var pointer: UnsafeMutablePointer<Float> = &result + shift
        cblas_scopy(Int32(data.count - shift), &data, 1, pointer, 1)
        if (shift > 1) {
            (0..<shift).forEach { index in result[shift - 1 - index] = self[lastIndex - index]}
        } else {
            result[0] = self[lastIndex]
        }
        return result
    }
}
