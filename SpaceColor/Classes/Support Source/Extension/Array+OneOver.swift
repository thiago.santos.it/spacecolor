//
//  Array+OneOver.swift
//  SpaceColor
//
//  Created by Thiago Santos on 31/12/22.
//

import Accelerate

extension [[Float]] {
    
    func oneOver() -> [[Float]] {
        return self.map { vDSP.divide(1, $0) }
    }
}

extension [Float] {
    func oneOver() -> [Float] {
        return vDSP.divide(1, self)
    }
}
