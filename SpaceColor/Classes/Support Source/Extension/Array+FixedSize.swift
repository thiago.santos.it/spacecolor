//
//  Array+FixedSize.swift
//  SpaceColor
//
//  Created by Thiago dos Santos on 13/12/22.
//

import Accelerate

extension Array<Float> {
    
    func to8Bits() -> [Int8] {
        let stride = vDSP_Stride(1)
        var result = [Int8](repeating: 0, count: self.count)
        let clipped = self.clip(limit: Float(Int8.max))
        vDSP_vfix8(clipped, stride, &result, stride, vDSP_Length(self.count))
        return result
    }
    
    func toU8Bits() -> [UInt8] {
        let stride = vDSP_Stride(1)
        var result = [UInt8](repeating: 0, count: self.count)
        let clipped = self.clip(limit: Float(UInt8.max))
        vDSP_vfixu8(clipped, stride, &result, stride, vDSP_Length(self.count))
        return result
    }
    
    func to16Bits() -> [Int16] {
        let stride = vDSP_Stride(1)
        var result = [Int16](repeating: 0, count: self.count)
        let clipped = self.clip(limit: Float(Int16.max))
        vDSP_vfix16(clipped, stride, &result, stride, vDSP_Length(self.count))
        return result
    }
    
    func toU16Bits() -> [UInt16] {
        let stride = vDSP_Stride(1)
        var result = [UInt16](repeating: 0, count: self.count)
        let clipped = self.clip(limit: Float(UInt16.max))
        vDSP_vfixu16(clipped, stride, &result, stride, vDSP_Length(self.count))
        return result
    }
    
    func to32Bits() -> [Int32] {
        let stride = vDSP_Stride(1)
        var result = [Int32](repeating: 0, count: self.count)
        let clipped = self.clip(limit: Float(Int32.max))
        vDSP_vfix32(clipped, stride, &result, stride, vDSP_Length(self.count))
        return result
    }
    
    func toU32Bits() -> [UInt32] {
        let stride = vDSP_Stride(1)
        var result = [UInt32](repeating: 0, count: self.count)
        let clipped = self.clip(limit: Float(UInt32.max))
        vDSP_vfixu32(clipped, stride, &result, stride, vDSP_Length(self.count))
        return result
    }
    
    private func clip(limit: Float) -> [Float] {
        let stride = vDSP_Stride(1)
        var min: Float = 0
        var max: Float = limit
        var clipped = self
        vDSP_vclip(self, stride, &min, &max, &clipped, stride, vDSP_Length(self.count))
        return clipped
    }
}

extension Array<Int8> {
    func toFloat() -> [Float] {
        var data = [Float](repeating: 0, count: self.count)
        vDSP.convertElements(of: self, to: &data)
        return data
    }
}

extension Array<UInt8> {
    func toFloat() -> [Float] {
        var data = [Float](repeating: 0, count: self.count)
        vDSP.convertElements(of: self, to: &data)
        return data
    }
}

extension Array<Int16> {
    func toFloat() -> [Float] {
        var data = [Float](repeating: 0, count: self.count)
        vDSP.convertElements(of: self, to: &data)
        return data
    }
}

extension Array<UInt16> {
    func toFloat() -> [Float] {
        var data = [Float](repeating: 0, count: self.count)
        vDSP.convertElements(of: self, to: &data)
        return data
    }
}

extension Array<Int32> {
    func toFloat() -> [Float] {
        var data = [Float](repeating: 0, count: self.count)
        vDSP.convertElements(of: self, to: &data)
        return data
    }
}

extension Array<UInt32> {
    func toFloat() -> [Float] {
        var data = [Float](repeating: 0, count: self.count)
        vDSP.convertElements(of: self, to: &data)
        return data
    }
}
