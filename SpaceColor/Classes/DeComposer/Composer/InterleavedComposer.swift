//
//  InterleavedComposer.swift
//  SpaceColor
//
//  Created by Thiago Santos on 17/12/22.
//  Copyright © 2022 CocoaPods. All rights reserved.
//

import Accelerate

class InterleavedComposer: Composer, Cacheable {
    
    typealias CacheHashable = Int
    typealias Composable = Interleaved
    
    private struct Storage {
        let template: [Float]
    }
    
    private let storage: Storage
    
    init(alloc: Int) {
        self.storage =  Storage(template: [Float](repeating: 0, count: alloc))
    }
    
    func from(planar: Planar) -> Interleaved {
        
        let planarComponents = planar.components1D.count
        
        let strideComponent = vDSP_Stride(planarComponents)
        let stride = vDSP_Stride(1)
        let lenght = vDSP_Length(planar.components1D[0].count)
        
        var result = self.storage.template
        
        (0..<planarComponents).forEach { component in
            let index = (planarComponents - 1) - component
            vDSP_vadd(planar.components1D[index], stride, result, strideComponent, &result, strideComponent, lenght)
            if (index > 0) {
                result = result >> 1
            }
        }
        
        return Interleaved(data: result, components: planarComponents, rowSize: planar.rowSize)
    }
    
    static func key(_ alloc: Int) -> String {
        return "InterleavedComposer(alloc:\(alloc))"
    }
}
