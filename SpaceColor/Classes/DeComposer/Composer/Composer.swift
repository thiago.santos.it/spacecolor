//
//  Composer.swift
//  SpaceColor
//
//  Created by Thiago Santos on 17/12/22.
//  Copyright © 2022 CocoaPods. All rights reserved.
//

protocol Composer {
    
    associatedtype Composable

    func from(planar: Planar) -> Composable
}
