//
//  BayerComposer.swift
//  SpaceColor
//
//  Created by Thiago Santos on 17/12/22.
//  Copyright © 2022 CocoaPods. All rights reserved.
//

import Accelerate

class BayerComposer: Composer {
    
    typealias Composable = Bayer
    
    let pattern: BayerPattern
    
    init(pattern: BayerPattern) {
        self.pattern = pattern
    }
    
    func from(planar: Planar) -> Bayer {
        
        var components1D = planar.components1D
        
        var result: [Float] = [Float](repeating: 0, count: components1D[0].count)

        let lenght = vDSP_Length(result.count)
        let stride = vDSP_Stride(1)
        
        vDSP_vadd(components1D[0], stride, components1D[1], stride, &result, stride, lenght)
        vDSP_vadd(components1D[2], stride, result, stride, &result, stride, lenght)
        
        return Bayer(data: result.split(by: planar.rowSize), pattern: pattern, rowSize: planar.rowSize)
    }
}
