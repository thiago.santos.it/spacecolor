//
//  Decomposer.swift
//  SpaceColor
//
//  Created by Thiago Santos on 17/12/22.
//  Copyright © 2022 CocoaPods. All rights reserved.
//

protocol Decomposer {

    associatedtype Decomposable

    func toPlanarFrom(decomposable: Decomposable) -> Planar
}
