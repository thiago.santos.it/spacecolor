//
//  InterleavedDecomposer.swift
//  SpaceColor
//
//  Created by Thiago dos Santos on 11/12/22.
//

import Accelerate

struct InterleavedDecomposer: Decomposer {
    
    typealias Decomposable = Interleaved
    
    init() {}

    func toPlanarFrom(decomposable interleaved: Interleaved) -> Planar {
       
        var mutableData = interleaved.data
        let components = interleaved.components
        
        let planeSize = mutableData.count / components
        let empty = [Float](repeating: 0, count: planeSize)
        
        
        var result = [VectorComponent](repeating: VectorComponent(), count: components)
        (0..<components).forEach { componentIndex in
            var plane = empty
            var pointer: UnsafePointer<Float> = &mutableData + componentIndex
            cblas_scopy(Int32(planeSize), pointer, Int32(components), &plane, 1)
            
            result[componentIndex] = plane
        }
        
        return Planar(components1D: result, rowSize: interleaved.rowSize)
    }
    
}
