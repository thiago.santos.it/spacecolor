//
//  BayerDecomposer.swift
//  SpaceColor
//
//  Created by Thiago dos Santos on 13/12/22.
//

import Accelerate

class BayerDecomposer: Decomposer, Cacheable {
    
    typealias CacheHashable = (Int, Int)
    typealias Decomposable = Bayer
    
    private struct Storage {
        struct RowMasks {
            var mainEven: [Float]
            var mainOdd: [Float]
            var firstEven: [Float]
            var secondOdd: [Float]
        }
        
        let masks: [BayerPattern: RowMasks]
        let emptyMatrix: [[Float]]
    }
    
    private let storage: Storage
    
    init(width: Int, height: Int) {
        let emptyMatrix = [[Float]](repeating: [Float](repeating: 0.0, count: width),
                                     count: height)
        
        var zeroFirst = [[Float]](repeating: [0, 1], count: width / 2).flatten()
        var oneFirst = [[Float]](repeating: [1, 0], count: width / 2).flatten()
        if (width % 2 != 0) {
            zeroFirst.append(0)
            oneFirst.append(1)
        }
        
        let masks: [BayerPattern: Storage.RowMasks] = [
            .MainFirstSecondMain : Storage.RowMasks(mainEven: oneFirst, mainOdd: zeroFirst, firstEven: zeroFirst, secondOdd: oneFirst),
            .FirstMainMainSecond : Storage.RowMasks(mainEven: zeroFirst, mainOdd: oneFirst, firstEven: oneFirst, secondOdd: zeroFirst)
        ]
        
        self.storage = Storage(masks: masks, emptyMatrix: emptyMatrix)
    }
    
    func toPlanarFrom(decomposable bayer: Bayer) -> Planar {
        var masks = storage.masks[bayer.pattern]!
        return Planar(components2D: [mainComponent(data: bayer.data, masks: masks),
                                     firstComponent(data: bayer.data, masks: masks),
                                     secondComponent(data: bayer.data, masks: masks)],
                      rowSize: bayer.data[0].count)
    }
    
    func toMainFrom(decomposable bayer: Bayer) -> MatrixComponent {
        var masks = storage.masks[bayer.pattern]!
        return mainComponent(data: bayer.data, masks: masks)
    }
    
    func toFirstFrom(decomposable bayer: Bayer) -> MatrixComponent {
        var masks = storage.masks[bayer.pattern]!
        return firstComponent(data: bayer.data, masks: masks)
    }
    
    func toSecondFrom(decomposable bayer: Bayer) -> MatrixComponent {
        var masks = storage.masks[bayer.pattern]!
        return secondComponent(data: bayer.data, masks: masks)               
    }
    
    private func mainComponent(data: [[Float]], masks: Storage.RowMasks) -> [[Float]] {
        var masks = masks
        var mainComponent = self.storage.emptyMatrix
        
        (0..<data.count).forEach { index in
            if (index % 2 == 0) {
                mainComponent[index] = vDSP.multiply(data[index], masks.mainEven)
            } else {
                mainComponent[index] = vDSP.multiply(data[index], masks.mainOdd)
            }
        }
        return mainComponent
    }
    
    private func firstComponent(data: [[Float]], masks: Storage.RowMasks) -> [[Float]] {
        
        var firstComponent = self.storage.emptyMatrix
        stride(from: 0, to: data.count, by: 2).forEach { index in
            firstComponent[index] = vDSP.multiply(data[index], masks.firstEven)
        }
        return firstComponent
        
    }
    
    private func secondComponent(data: [[Float]], masks: Storage.RowMasks) -> [[Float]] {
        
        var secondComponent = self.storage.emptyMatrix
        stride(from: 1, to: data.count, by: 2).forEach { index in
            secondComponent[index] = vDSP.multiply(data[index], masks.secondOdd)
        }
        return secondComponent
    }
    
    static func key(_ size: (Int, Int)) -> String {
        return "BayerDecomposer(width:\(size.0), height:\(size.1))"
    }
}
