//
//  Planar.swift
//  SpaceColor
//
//  Created by Thiago dos Santos on 26/12/22.
//

typealias VectorComponent = [Float]
typealias MatrixComponent = [[Float]]

class Planar {
    
    private(set) lazy var components1D: [VectorComponent] = {
        return components2D.map { $0.flatten() }
    }()
    
    private(set) lazy var components2D: [MatrixComponent] = {
        return components1D.map { $0.split(by: rowSize) }
    }()
    
    let rowSize: Int
    
    init(components1D: [VectorComponent], rowSize: Int) {
        self.rowSize = rowSize
        self.components1D = components1D
    }
    
    init(components2D: [MatrixComponent], rowSize: Int) {
        self.rowSize = rowSize
        self.components2D = components2D
    }
}
