//
//  Bayer.swift
//  SpaceColor
//
//  Created by Thiago dos Santos on 27/12/22.
//

typealias BayerData = [[Float]]

enum BayerPattern {
    case MainFirstSecondMain
    case FirstMainMainSecond
}

struct Bayer {
    let data: BayerData
    let pattern: BayerPattern
    let rowSize: Int
}
