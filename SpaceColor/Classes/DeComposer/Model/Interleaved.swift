//
//  Interleaved.swift
//  SpaceColor
//
//  Created by Thiago dos Santos on 27/12/22.
//

typealias InterleavedData = [Float]

struct Interleaved {
    let data: InterleavedData
    let components: Int
    let rowSize: Int
}

