//
//  DeComposerCache.swift
//  SpaceColor
//
//  Created by Thiago dos Santos on 09/01/23.
//



class DeComposerCache {
    
    private let interleavedComposerCache = Cache<InterleavedComposer>()
    private let bayerDecomposerCache = Cache<BayerDecomposer>()
    
    func getInterleavedComposer(alloc: Int) -> InterleavedComposer {
        return interleavedComposerCache.load(key: InterleavedComposer.key(alloc)) {
            InterleavedComposer(alloc: alloc)
        }
    }
    
    func getInterleavedDecomposer() -> InterleavedDecomposer {
        return InterleavedDecomposer()
    }
    
    func getBayerComposer(pattern: BayerPattern) -> BayerComposer {
        return BayerComposer(pattern: pattern)
    }
    
    func getBayerDecomposer(width: Int, height: Int) -> BayerDecomposer {
        return bayerDecomposerCache.load(key: BayerDecomposer.key((width, height))) {
            BayerDecomposer(width: width, height: height)
        }
    }
}
