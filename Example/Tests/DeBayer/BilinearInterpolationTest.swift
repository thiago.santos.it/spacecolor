//
//  BilinearInterpolationTest.swift
//  SpaceColor_Tests
//
//  Created by Thiago Santos on 17/12/22.
//  Copyright © 2022 CocoaPods. All rights reserved.
//

import XCTest
import Nimble
import Accelerate
@testable import SpaceColor

final class BilinearInterpolationTest: XCTestCase {
    
    func test() {
        let data: [[Float]] = [[2, 1, 2, 1, 2, 1, 2, 1, 2, 1, 2, 1, 2, 1, 2, 1],
                               [1, 3, 1, 3, 1, 3, 1, 3, 1, 3, 1, 3, 1, 3, 1, 3],
                               [2, 1, 2, 1, 2, 1, 2, 1, 2, 1, 2, 1, 2, 1, 2, 1],
                               [1, 3, 1, 3, 1, 3, 1, 3, 1, 3, 1, 3, 1, 3, 1, 3],
                               [2, 1, 2, 1, 2, 1, 2, 1, 2, 1, 2, 1, 2, 1, 2, 1],
                               [1, 3, 1, 3, 1, 3, 1, 3, 1, 3, 1, 3, 1, 3, 1, 3]]
        
        let colorSpace = ColorSpace.RGGB
        let width = data[0].count
        let height = data.count
        let imageFormat = ImageFormat(width: width, height: height, colorSpace: colorSpace)
        
        
        guard let context = DebayeringContext(imageFormat: imageFormat) else {
            XCTFail("Debayering context not created")
            return
        }
        
        let interpolation = BilinearInterpolation(context: context)
        let decomposer = BayerDecomposer(width: width, height: height)
        let planar = decomposer.toPlanarFrom(decomposable: Bayer(data: data, pattern: context.pattern, rowSize: imageFormat.rowSize()))
        let result = interpolation.apply(planar: planar)
        
        expect(result.components1D[0]).to(equal([Float](repeating: 1, count: imageFormat.size())))
        expect(result.components1D[1]).to(equal([Float](repeating: 2, count: imageFormat.size())))
        expect(result.components1D[2]).to(equal([Float](repeating: 3, count: imageFormat.size())))
    }
    
    func testOdd() {
        let data: [[Float]] = [[2, 1, 2, 1, 2, 1, 2, 1, 2, 1, 2, 1, 2, 1, 2, 1, 2],
                               [1, 3, 1, 3, 1, 3, 1, 3, 1, 3, 1, 3, 1, 3, 1, 3, 1],
                               [2, 1, 2, 1, 2, 1, 2, 1, 2, 1, 2, 1, 2, 1, 2, 1, 2],
                               [1, 3, 1, 3, 1, 3, 1, 3, 1, 3, 1, 3, 1, 3, 1, 3, 1],
                               [2, 1, 2, 1, 2, 1, 2, 1, 2, 1, 2, 1, 2, 1, 2, 1, 2],
                               [1, 3, 1, 3, 1, 3, 1, 3, 1, 3, 1, 3, 1, 3, 1, 3, 1],
                               [2, 1, 2, 1, 2, 1, 2, 1, 2, 1, 2, 1, 2, 1, 2, 1, 2]]
        
        let colorSpace = ColorSpace.RGGB
        let width = data[0].count
        let height = data.count
        let imageFormat = ImageFormat(width: width, height: height, colorSpace: colorSpace)
        
        
        guard let context = DebayeringContext(imageFormat: imageFormat) else {
            XCTFail("Debayering context not created")
            return
        }
        
        let interpolation = BilinearInterpolation(context: context)
        let decomposer = BayerDecomposer(width: width, height: height)
        let planar = decomposer.toPlanarFrom(decomposable: Bayer(data: data, pattern: context.pattern, rowSize: imageFormat.rowSize()))
        let result = interpolation.apply(planar: planar)
        
        expect(result.components1D[0]).to(equal([Float](repeating: 1, count: imageFormat.size())))
        expect(result.components1D[1]).to(equal([Float](repeating: 2, count: imageFormat.size())))
        expect(result.components1D[2]).to(equal([Float](repeating: 3, count: imageFormat.size())))
    }
    
    func testPerformance() {
        let width = 1920
        let height = 1080
        let data = [[Float]](repeating: [Float](repeating: 1, count: width), count: height)
        
        let colorSpace = ColorSpace.RGGB
        let imageFormat = ImageFormat(width: width, height: height, colorSpace: colorSpace)
        
        guard let context = DebayeringContext(imageFormat: imageFormat) else {
            XCTFail("Debayering context not created")
            return
        }

        var result: Planar? = nil
        let interpolation = BilinearInterpolation(context: context)
        let decomposer = BayerDecomposer(width: width, height: height)
        let planar = decomposer.toPlanarFrom(decomposable: Bayer(data: data, pattern: context.pattern, rowSize: imageFormat.rowSize()))
        
        self.measure {
            result = interpolation.apply(planar: planar)
        }
        expect(result).toNot(beNil())
    }
    
}
