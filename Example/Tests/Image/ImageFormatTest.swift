//
//  ImageFormatTest.swift
//  SpaceColor_Tests
//
//  Created by Thiago dos Santos on 22/12/22.
//  Copyright © 2022 CocoaPods. All rights reserved.
//

import XCTest
import Nimble
@testable import SpaceColor

final class ImageFormatTest: XCTestCase {

    let width = 11
    let height = 7
    let numberOfComponent = 3

    func testRowSize() throws {
        let colorSpace = ColorSpace.RGB
        let imageFormat = ImageFormat(width: width, height: height, colorSpace: colorSpace)
        expect(imageFormat.rowSize()).to(equal(width * numberOfComponent))
    }
    
    func testSize() throws {
        let colorSpace = ColorSpace.RGB
        let imageFormat = ImageFormat(width: width, height: height, colorSpace: colorSpace)
        expect(imageFormat.size()).to(equal(height * width * numberOfComponent))
    }
    
    func testPixels() throws {
        let colorSpace = ColorSpace.RGB
        let imageFormat = ImageFormat(width: width, height: height, colorSpace: colorSpace)
        expect(imageFormat.pixels()).to(equal(height * width))
    }
    
    func testWithWidth() throws {
        let colorSpace = ColorSpace.RGB
        let imageFormat = ImageFormat(width: width, height: height, colorSpace: colorSpace).with(width: 1)
        expect(imageFormat.width).to(equal(1))
    }
    
    func testWithHeight() throws {
        let colorSpace = ColorSpace.RGB
        let imageFormat = ImageFormat(width: width, height: height, colorSpace: colorSpace).with(height: 1)
        expect(imageFormat.height).to(equal(1))
    }
    
    func testWithColorSpace() throws {
        let colorSpace = ColorSpace.RGB
        let imageFormat = ImageFormat(width: width, height: height, colorSpace: colorSpace).with(colorSpace: ColorSpace.RGBA)
        expect(imageFormat.colorSpace).to(equal(ColorSpace.RGBA))
    }

}
