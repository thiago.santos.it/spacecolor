//
//  ImageBitsTest.swift
//  SpaceColor_Tests
//
//  Created by Thiago dos Santos on 02/01/23.
//  Copyright © 2023 CocoaPods. All rights reserved.
//

import XCTest
import Nimble
@testable import SpaceColor

final class ImageBitsTest: XCTestCase {
    
    func testMax() {
        expect(ImageBits.S16.max()).to(equal(Float(Int16.max)))
        expect(ImageBits.U16.max()).to(equal(Float(UInt16.max)))
        expect(ImageBits.S8.max()).to(equal(Float(Int8.max)))
        expect(ImageBits.U8.max()).to(equal(Float(UInt8.max)))
    }    
}
