//
//  ColorSpaceTest.swift
//  SpaceColor_Tests
//
//  Created by Thiago dos Santos on 29/12/22.
//  Copyright © 2022 CocoaPods. All rights reserved.
//

import XCTest
import Nimble
@testable import SpaceColor

final class ColorSpaceTest: XCTestCase {

    func testPixelSize() {
        expect(ColorSpace.GS.pixelSize).to(equal(1))
        
        expect(ColorSpace.AGS.pixelSize).to(equal(2))
        expect(ColorSpace.GSA.pixelSize).to(equal(2))
        
        expect(ColorSpace.RGB.pixelSize).to(equal(3))
        expect(ColorSpace.BGR.pixelSize).to(equal(3))
        expect(ColorSpace.CMY.pixelSize).to(equal(3))
        
        expect(ColorSpace.ACMY.pixelSize).to(equal(4))
        expect(ColorSpace.CMYA.pixelSize).to(equal(4))
        expect(ColorSpace.RGBA.pixelSize).to(equal(4))
        expect(ColorSpace.ARGB.pixelSize).to(equal(4))
        expect(ColorSpace.BGRA.pixelSize).to(equal(4))
        expect(ColorSpace.ABGR.pixelSize).to(equal(4))
        expect(ColorSpace.CMYK.pixelSize).to(equal(4))
        
        expect(ColorSpace.CMYKA.pixelSize).to(equal(5))
        expect(ColorSpace.ACMYK.pixelSize).to(equal(5))
        
        expect(ColorSpace.RGGB.pixelSize).to(equal(1))
        expect(ColorSpace.BGGR.pixelSize).to(equal(1))
        expect(ColorSpace.CYYM.pixelSize).to(equal(1))
        expect(ColorSpace.MYYC.pixelSize).to(equal(1))
        expect(ColorSpace.GRBG.pixelSize).to(equal(1))
        expect(ColorSpace.GBRG.pixelSize).to(equal(1))
        expect(ColorSpace.YCMY.pixelSize).to(equal(1))
        expect(ColorSpace.YMCY.pixelSize).to(equal(1))
    }
    
    func testComponents() {
        expect(ColorSpace.GS.pixelSize).to(equal(1))
        
        expect(ColorSpace.AGS.pixelSize).to(equal(2))
        expect(ColorSpace.GSA.pixelSize).to(equal(2))
        
        expect(ColorSpace.RGB.components).to(equal(3))
        expect(ColorSpace.BGR.components).to(equal(3))
        expect(ColorSpace.CMY.components).to(equal(3))
        
        expect(ColorSpace.ACMY.components).to(equal(4))
        expect(ColorSpace.CMYA.components).to(equal(4))
        expect(ColorSpace.RGBA.components).to(equal(4))
        expect(ColorSpace.ARGB.components).to(equal(4))
        expect(ColorSpace.BGRA.components).to(equal(4))
        expect(ColorSpace.ABGR.components).to(equal(4))
        expect(ColorSpace.CMYK.components).to(equal(4))
        
        expect(ColorSpace.CMYKA.components).to(equal(5))
        expect(ColorSpace.ACMYK.components).to(equal(5))
        
        expect(ColorSpace.RGGB.components).to(equal(3))
        expect(ColorSpace.BGGR.components).to(equal(3))
        expect(ColorSpace.CYYM.components).to(equal(3))
        expect(ColorSpace.MYYC.components).to(equal(3))
        expect(ColorSpace.GRBG.components).to(equal(3))
        expect(ColorSpace.GBRG.components).to(equal(3))
        expect(ColorSpace.YCMY.components).to(equal(3))
        expect(ColorSpace.YMCY.components).to(equal(3))
    }
    
    func testIsBayer() {
        expect(ColorSpace.GS.bayer).to(equal(false))
        
        expect(ColorSpace.AGS.bayer).to(equal(false))
        expect(ColorSpace.GSA.bayer).to(equal(false))
        
        expect(ColorSpace.RGB.bayer).to(equal(false))
        expect(ColorSpace.BGR.bayer).to(equal(false))
        expect(ColorSpace.CMY.bayer).to(equal(false))
        
        expect(ColorSpace.ACMY.bayer).to(equal(false))
        expect(ColorSpace.CMYA.bayer).to(equal(false))
        
        expect(ColorSpace.RGBA.bayer).to(equal(false))
        expect(ColorSpace.ARGB.bayer).to(equal(false))
        expect(ColorSpace.BGRA.bayer).to(equal(false))
        expect(ColorSpace.ABGR.bayer).to(equal(false))
        expect(ColorSpace.CMYK.bayer).to(equal(false))
        
        expect(ColorSpace.CMYKA.bayer).to(equal(false))
        expect(ColorSpace.ACMYK.bayer).to(equal(false))
        
        expect(ColorSpace.RGGB.bayer).to(equal(true))
        expect(ColorSpace.BGGR.bayer).to(equal(true))
        expect(ColorSpace.CYYM.bayer).to(equal(true))
        expect(ColorSpace.MYYC.bayer).to(equal(true))
        expect(ColorSpace.GRBG.bayer).to(equal(true))
        expect(ColorSpace.GBRG.bayer).to(equal(true))
        expect(ColorSpace.YCMY.bayer).to(equal(true))
        expect(ColorSpace.YMCY.bayer).to(equal(true))
    }
    
    func testIsInterleaved() {
        expect(ColorSpace.GS.interleaved).to(equal(true))
        
        expect(ColorSpace.AGS.interleaved).to(equal(true))
        expect(ColorSpace.GSA.interleaved).to(equal(true))
        
        expect(ColorSpace.RGB.interleaved).to(equal(true))
        expect(ColorSpace.BGR.interleaved).to(equal(true))
        expect(ColorSpace.CMY.interleaved).to(equal(true))
        
        expect(ColorSpace.ACMY.interleaved).to(equal(true))
        expect(ColorSpace.CMYA.interleaved).to(equal(true))
        
        expect(ColorSpace.RGBA.interleaved).to(equal(true))
        expect(ColorSpace.ARGB.interleaved).to(equal(true))
        expect(ColorSpace.BGRA.interleaved).to(equal(true))
        expect(ColorSpace.ABGR.interleaved).to(equal(true))
        expect(ColorSpace.CMYK.interleaved).to(equal(true))
        
        expect(ColorSpace.CMYKA.interleaved).to(equal(true))
        expect(ColorSpace.ACMYK.interleaved).to(equal(true))
        
        expect(ColorSpace.RGGB.interleaved).to(equal(false))
        expect(ColorSpace.BGGR.interleaved).to(equal(false))
        expect(ColorSpace.CYYM.interleaved).to(equal(false))
        expect(ColorSpace.MYYC.interleaved).to(equal(false))
        expect(ColorSpace.GBRG.interleaved).to(equal(false))
        expect(ColorSpace.YCMY.interleaved).to(equal(false))
        expect(ColorSpace.YMCY.interleaved).to(equal(false))
    }
    
    func testHasAlpha() {
        expect(ColorSpace.GS.hasAlpha).to(equal(false))
        
        expect(ColorSpace.AGS.hasAlpha).to(equal(true))
        expect(ColorSpace.GSA.hasAlpha).to(equal(true))
        
        expect(ColorSpace.RGB.hasAlpha).to(equal(false))
        expect(ColorSpace.BGR.hasAlpha).to(equal(false))
        expect(ColorSpace.CMY.hasAlpha).to(equal(false))
        
        expect(ColorSpace.ACMY.hasAlpha).to(equal(true))
        expect(ColorSpace.CMYA.hasAlpha).to(equal(true))
        
        expect(ColorSpace.RGBA.hasAlpha).to(equal(true))
        expect(ColorSpace.ARGB.hasAlpha).to(equal(true))
        expect(ColorSpace.BGRA.hasAlpha).to(equal(true))
        expect(ColorSpace.ABGR.hasAlpha).to(equal(true))
        expect(ColorSpace.CMYK.hasAlpha).to(equal(false))
        
        expect(ColorSpace.CMYKA.hasAlpha).to(equal(true))
        expect(ColorSpace.ACMYK.hasAlpha).to(equal(true))
        
        expect(ColorSpace.RGGB.hasAlpha).to(equal(false))
        expect(ColorSpace.BGGR.hasAlpha).to(equal(false))
        expect(ColorSpace.CYYM.hasAlpha).to(equal(false))
        expect(ColorSpace.MYYC.hasAlpha).to(equal(false))
        expect(ColorSpace.GBRG.hasAlpha).to(equal(false))
        expect(ColorSpace.YCMY.hasAlpha).to(equal(false))
        expect(ColorSpace.YMCY.hasAlpha).to(equal(false))
    }
    
    func testHasAlphaAtBegging() {
        expect(ColorSpace.GS.hasAlphaAtBegging).to(equal(false))
        
        expect(ColorSpace.AGS.hasAlphaAtBegging).to(equal(true))
        expect(ColorSpace.GSA.hasAlphaAtBegging).to(equal(false))
        
        expect(ColorSpace.RGB.hasAlphaAtBegging).to(equal(false))
        expect(ColorSpace.BGR.hasAlphaAtBegging).to(equal(false))
        expect(ColorSpace.CMY.hasAlphaAtBegging).to(equal(false))
        
        expect(ColorSpace.ACMY.hasAlphaAtBegging).to(equal(true))
        expect(ColorSpace.CMYA.hasAlphaAtBegging).to(equal(false))
        
        expect(ColorSpace.RGBA.hasAlphaAtBegging).to(equal(false))
        expect(ColorSpace.ARGB.hasAlphaAtBegging).to(equal(true))
        expect(ColorSpace.BGRA.hasAlphaAtBegging).to(equal(false))
        expect(ColorSpace.ABGR.hasAlphaAtBegging).to(equal(true))
        expect(ColorSpace.CMYK.hasAlphaAtBegging).to(equal(false))
        
        expect(ColorSpace.CMYKA.hasAlphaAtBegging).to(equal(false))
        expect(ColorSpace.ACMYK.hasAlphaAtBegging).to(equal(true))
        
        expect(ColorSpace.RGGB.hasAlphaAtBegging).to(equal(false))
        expect(ColorSpace.BGGR.hasAlphaAtBegging).to(equal(false))
        expect(ColorSpace.CYYM.hasAlphaAtBegging).to(equal(false))
        expect(ColorSpace.MYYC.hasAlphaAtBegging).to(equal(false))
        expect(ColorSpace.GBRG.hasAlphaAtBegging).to(equal(false))
        expect(ColorSpace.YCMY.hasAlphaAtBegging).to(equal(false))
        expect(ColorSpace.YMCY.hasAlphaAtBegging).to(equal(false))
    }
    
    func testHasAlphaAtEnd() {
        expect(ColorSpace.GS.hasAlphaAtEnd).to(equal(false))
        
        expect(ColorSpace.AGS.hasAlphaAtEnd).to(equal(false))
        expect(ColorSpace.GSA.hasAlphaAtEnd).to(equal(true))
        
        expect(ColorSpace.RGB.hasAlphaAtEnd).to(equal(false))
        expect(ColorSpace.BGR.hasAlphaAtEnd).to(equal(false))
        expect(ColorSpace.CMY.hasAlphaAtEnd).to(equal(false))
        
        expect(ColorSpace.ACMY.hasAlphaAtEnd).to(equal(false))
        expect(ColorSpace.CMYA.hasAlphaAtEnd).to(equal(true))
        
        expect(ColorSpace.RGBA.hasAlphaAtEnd).to(equal(true))
        expect(ColorSpace.ARGB.hasAlphaAtEnd).to(equal(false))
        expect(ColorSpace.BGRA.hasAlphaAtEnd).to(equal(true))
        expect(ColorSpace.ABGR.hasAlphaAtEnd).to(equal(false))
        expect(ColorSpace.CMYK.hasAlphaAtEnd).to(equal(false))
        
        expect(ColorSpace.CMYKA.hasAlphaAtEnd).to(equal(true))
        expect(ColorSpace.ACMYK.hasAlphaAtEnd).to(equal(false))
        
        expect(ColorSpace.RGGB.hasAlphaAtEnd).to(equal(false))
        expect(ColorSpace.BGGR.hasAlphaAtEnd).to(equal(false))
        expect(ColorSpace.CYYM.hasAlphaAtEnd).to(equal(false))
        expect(ColorSpace.MYYC.hasAlphaAtEnd).to(equal(false))
        expect(ColorSpace.GBRG.hasAlphaAtEnd).to(equal(false))
        expect(ColorSpace.YCMY.hasAlphaAtEnd).to(equal(false))
        expect(ColorSpace.YMCY.hasAlphaAtEnd).to(equal(false))
    }
    
    func testBayerPattern() {
        expect(ColorSpace.GS.bayerPattern).to(beNil())
        
        expect(ColorSpace.AGS.bayerPattern).to(beNil())
        expect(ColorSpace.GSA.bayerPattern).to(beNil())
        
        expect(ColorSpace.RGB.bayerPattern).to(beNil())
        expect(ColorSpace.BGR.bayerPattern).to(beNil())
        expect(ColorSpace.CMY.bayerPattern).to(beNil())
        
        expect(ColorSpace.ACMY.bayerPattern).to(beNil())
        expect(ColorSpace.CMYA.bayerPattern).to(beNil())
        
        expect(ColorSpace.RGBA.bayerPattern).to(beNil())
        expect(ColorSpace.ARGB.bayerPattern).to(beNil())
        expect(ColorSpace.BGRA.bayerPattern).to(beNil())
        expect(ColorSpace.ABGR.bayerPattern).to(beNil())
        expect(ColorSpace.CMYK.bayerPattern).to(beNil())
        
        expect(ColorSpace.CMYKA.bayerPattern).to(beNil())
        expect(ColorSpace.ACMYK.bayerPattern).to(beNil())
        
        expect(ColorSpace.RGGB.bayerPattern).to(equal(.FirstMainMainSecond))
        expect(ColorSpace.BGGR.bayerPattern).to(equal(.FirstMainMainSecond))
        expect(ColorSpace.CYYM.bayerPattern).to(equal(.FirstMainMainSecond))
        expect(ColorSpace.MYYC.bayerPattern).to(equal(.FirstMainMainSecond))
        expect(ColorSpace.GBRG.bayerPattern).to(equal(.MainFirstSecondMain))
        expect(ColorSpace.YCMY.bayerPattern).to(equal(.MainFirstSecondMain))
        expect(ColorSpace.YMCY.bayerPattern).to(equal(.MainFirstSecondMain))
    }
}
