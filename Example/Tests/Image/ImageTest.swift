//
//  Image.swift
//  SpaceColor_Tests
//
//  Created by Thiago Santos on 31/12/22.
//  Copyright © 2022 CocoaPods. All rights reserved.
//

import Foundation
/*
 func testPerformance() throws {
     let data = ImageLoader().imageData(forResource: "mona", withExtension: "bmp")
     
     let imageFormatRGB = ImageFormat(width: 428, height: 640, numberOfComponents: 4)
     let imageFormatBayer = imageFormatRGB.withNumberOfComponents(numberOfComponents: 1)
     
     let colorSpace = ColorSpace()
     
     //These objects should be cached
     let interleavedComposer =  InterleavedComposer(imageFormat: imageFormatRGB, extraComponents: [255], extraAtBeginning: false)
     let interleavedDecomposer = InterleavedDecomposer(imageFormat: imageFormatRGB, ignoreAtBeginning: false)
     let debayer = NearestNeightborInterpolation(imageFormat: imageFormatBayer)
     
     self.measure {
         
         let srcRGB = interleavedDecomposer.toPlanar3C(raw: data.toFloat())
         let srcBayer = colorSpace.rgbToBGGR(planar: srcRGB, newImageFormat: imageFormatBayer)
         
         let result = debayer.apply(planar: srcBayer)
         let outRGB = colorSpace.bggrToRGB(planar: result, newImageFormat: imageFormatRGB)
         
         let rgbData = interleavedComposer.fromPlanar3C(planar: outRGB)
         let cgImage = ImageLoader().createImage(raw: rgbData.toU8Bits(), imageFormat: imageFormatRGB)
        
         expect(cgImage).toNot(beNil())
     }
 }*/
