//
//  ImageLoader.swift
//  SpaceColor_Tests
//
//  Created by Thiago dos Santos on 15/12/22.
//  Copyright © 2022 CocoaPods. All rights reserved.
//

import Foundation
import AppKit
@testable import SpaceColor

extension NSImage {
   
    func bytes() -> [UInt8]? {
        guard let cgImage = self.asCGImage() else { return nil }
        let rawData = self.copyImagePixels(cgImage: cgImage)
        var result = [UInt8](repeating: 0, count: CFDataGetLength(rawData))
        CFDataGetBytes(rawData, CFRangeMake(0, CFDataGetLength(rawData)), &result)
        return result
    }
    
    private func copyImagePixels(cgImage: CGImage) -> CFData? {
        guard let provider = cgImage.dataProvider else { return nil }
        return provider.data
    }
}
