//
//  CreateBayer.swift
//  SpaceColor_Tests
//
//  Created by Thiago dos Santos on 04/01/23.
//  Copyright © 2023 CocoaPods. All rights reserved.
//
import XCTest
@testable import SpaceColor

final class CreateBayer: XCTestCase {
    
    func testDoIt() throws {
        let source: String? = nil//= "mona-rgba"
        let components = 4
        let width = 329
        let height = 436
        let fileExtension = "tiff"
        guard let source = source else { return }
        let data = ImageLoader().imageData(forResource: source, withExtension: fileExtension)
        let planar = InterleavedDecomposer().toPlanarFrom(decomposable: Interleaved(data: data.toFloat(), components: components, rowSize: width))
        
        //FirstMainMainSecond
        var redBayer = BayerDecomposer(width: width, height: height).toPlanarFrom(decomposable: Bayer(data: planar.components2D[0], pattern: .FirstMainMainSecond, rowSize: width))
        var greenBayer = BayerDecomposer(width: width, height: height).toPlanarFrom(decomposable: Bayer(data: planar.components2D[1], pattern: .FirstMainMainSecond, rowSize: width))
        var blueBayer = BayerDecomposer(width: width, height: height).toPlanarFrom(decomposable: Bayer(data: planar.components2D[2], pattern: .FirstMainMainSecond, rowSize: width))
        //-RGGB
        var main = greenBayer.components2D[0]
        var first = redBayer.components2D[1]
        var second = blueBayer.components2D[2]
        var fileData = Data(BayerComposer(pattern: .FirstMainMainSecond).from(planar: Planar(components2D: [main, first, second], rowSize: width)).data.flatten().toU8Bits())
        var filePath = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0].appendingPathComponent("\(source)-\(width)x\(height)-rggb.raw")
        try fileData.write(to: filePath)
        
        //-BGGR
        main = greenBayer.components2D[0]
        first = blueBayer.components2D[1]
        second = redBayer.components2D[2]
        fileData = Data(BayerComposer(pattern: .FirstMainMainSecond).from(planar: Planar(components2D: [main, first, second], rowSize: width)).data.flatten().toU8Bits())
        filePath = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0].appendingPathComponent("\(source)-\(width)x\(height)-bggr.raw")
        try fileData.write(to: filePath)
        
        //MainFirstSecondMain
        redBayer = BayerDecomposer(width: width, height: height).toPlanarFrom(decomposable: Bayer(data: planar.components2D[0], pattern: .MainFirstSecondMain, rowSize: width))
        greenBayer = BayerDecomposer(width: width, height: height).toPlanarFrom(decomposable: Bayer(data: planar.components2D[1], pattern: .MainFirstSecondMain, rowSize: width))
        blueBayer = BayerDecomposer(width: width, height: height).toPlanarFrom(decomposable: Bayer(data: planar.components2D[2], pattern: .MainFirstSecondMain, rowSize: width))
        
        //-GRBG
        main = greenBayer.components2D[0]
        first = redBayer.components2D[1]
        second = blueBayer.components2D[2]
        fileData = Data(BayerComposer(pattern: .FirstMainMainSecond).from(planar: Planar(components2D: [main, first, second], rowSize: width)).data.flatten().toU8Bits())
        filePath = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0].appendingPathComponent("\(source)-\(width)x\(height)-grbg.raw")
        try fileData.write(to: filePath)
        
        //GBRG
        main = greenBayer.components2D[0]
        first = blueBayer.components2D[1]
        second = redBayer.components2D[2]
        fileData = Data(BayerComposer(pattern: .FirstMainMainSecond).from(planar: Planar(components2D: [main, first, second], rowSize: width)).data.flatten().toU8Bits())
        filePath = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0].appendingPathComponent("\(source)-\(width)x\(height)-gbrg.raw")
        try fileData.write(to: filePath)
        
    }
}
