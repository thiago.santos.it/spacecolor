//
//  ImageLoader.swift
//  SpaceColor_Tests
//
//  Created by Thiago dos Santos on 23/12/22.
//  Copyright © 2022 CocoaPods. All rights reserved.
//

import Foundation
import Accelerate
import XCTest

@testable import SpaceColor

class ImageLoader {
    
    func imageData(forResource resource: String, withExtension ext: String) -> [UInt8] {
        guard let imageUrl = Bundle(for: ImageLoader.self).url(forResource: resource, withExtension: ext) else {
            XCTFail("Sample image file not found for test, skipping!")
            return []
        }
        
        guard let nsImage = NSImage(contentsOfFile: imageUrl.path) else {
            XCTFail("Sample image file not created for test, skipping!")
            return []
        }
        
        guard let data = nsImage.bytes() else {
            XCTFail("Sample image data not extracted for test, skipping!")
            return []
        }
        return data
    }
    
    func createImage(raw: [UInt8], imageFormat: ImageFormat) -> CGImage? {
        do {
            var imageBuffer: vImage_Buffer? = nil
            raw.withUnsafeBufferPointer { buffer in
                imageBuffer = vImage_Buffer(data: UnsafeMutableRawPointer(mutating: buffer.baseAddress),
                                            height: vImagePixelCount(imageFormat.height),
                                            width: vImagePixelCount(imageFormat.width),
                                            rowBytes: imageFormat.rowSize())
            }
                
            guard let imageBuffer = imageBuffer else {
                XCTFail("Image buffer not created!")
                return nil
            }
            if let format = vImage_CGImageFormat(
                bitsPerComponent: 8,
                bitsPerPixel: 32,
                colorSpace: CGColorSpaceCreateDeviceRGB(),
                bitmapInfo: CGBitmapInfo(rawValue: CGImageAlphaInfo.last.rawValue),
                renderingIntent: .defaultIntent) {
                return try imageBuffer.createCGImage(format: format)
            }
        } catch {
            XCTFail("Error creating image \(error)")
        }
        return nil
    }
}
