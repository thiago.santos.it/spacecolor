//
//  Array+OneOverTest.swift
//  SpaceColor_Tests
//
//  Created by Thiago Santos on 01/01/23.
//  Copyright © 2023 CocoaPods. All rights reserved.
//


import XCTest
import Nimble
@testable import SpaceColor

final class ArrayOneOverTest: XCTestCase {

    func test() throws {
        let array: [Float] = [1, 2, 3, 4, 5]
        let resut: [Float] = [1/1, 1/2, 1/3, 1/4, 1/5]
        expect(array.oneOver()).to(equal(resut))
    }
}
