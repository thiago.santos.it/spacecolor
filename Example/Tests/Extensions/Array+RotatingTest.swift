//
//  Array+RotatingTest.swift
//  SpaceColor_Tests
//
//  Created by Thiago Santos on 18/12/22.
//  Copyright © 2022 CocoaPods. All rights reserved.
//

import XCTest
import Nimble
@testable import SpaceColor

final class ArrayRotatingTest: XCTestCase {

    func test() throws {
        let array: [Float] = [1, 2, 3, 4, 5]
        expect(array +<< 1).to(equal([2, 3, 4, 5, 1]))
        expect(array +<< 2).to(equal([3, 4, 5, 1, 2]))
        expect(array >>+ 1).to(equal([5, 1, 2, 3, 4]))
        expect(array >>+ 2).to(equal([4, 5, 1, 2, 3]))
    }
}

