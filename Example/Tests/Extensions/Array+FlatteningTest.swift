//
//  Array+FlatteningTest.swift
//  SpaceColor_Tests
//
//  Created by Thiago Santos on 18/12/22.
//  Copyright © 2022 CocoaPods. All rights reserved.
//

import XCTest
import Nimble
@testable import SpaceColor

final class ArrayFlatteningTest: XCTestCase {

    func test() throws {
        let array: [Float] = [1, 2, 3, 4, 5]
        let arrayL2: [[Float]] = [[1, 2], [3, 4, 5]]
        expect(arrayL2.flatten()).to(equal(array))
    }
}


