//
//  Array+FixedSizeTest.swift
//  SpaceColor_Tests
//
//  Created by Thiago Santos on 17/12/22.
//  Copyright © 2022 CocoaPods. All rights reserved.
//

import XCTest
import Nimble
@testable import SpaceColor

final class ArrayFixedSizeTest: XCTestCase {
    
    private let arrayUInt8: [UInt8] = [1, 2, 3, 4, 5]
    private let arrayUInt16: [UInt16] = [1, 2, 3, 4, 5]
    private let arrayUInt32: [UInt32] = [1, 2, 3, 4, 5]
    
    private let arrayInt8: [Int8] = [1, 2, 3, 4, 5]
    private let arrayInt16: [Int16] = [1, 2, 3, 4, 5]
    private let arrayInt32: [Int32] = [1, 2, 3, 4, 5]
    
    private let arrayFloat: [Float] = [1, 2, 3, 4, 5]
    
    func testFixedToFloat() throws {
        expect(self.arrayUInt8.toFloat()).to(equal(arrayFloat))
        expect(self.arrayUInt16.toFloat()).to(equal(arrayFloat))
        expect(self.arrayUInt32.toFloat()).to(equal(arrayFloat))

        expect(self.arrayInt8.toFloat()).to(equal(arrayFloat))
        expect(self.arrayInt16.toFloat()).to(equal(arrayFloat))
        expect(self.arrayInt32.toFloat()).to(equal(arrayFloat))
    }
    
    func testFloatToFixed() {
        expect(self.arrayFloat.toU8Bits()).to(equal(arrayUInt8))
        expect(self.arrayFloat.toU16Bits()).to(equal(arrayUInt16))
        expect(self.arrayFloat.toU32Bits()).to(equal(arrayUInt32))
        
        expect(self.arrayFloat.to8Bits()).to(equal(arrayInt8))
        expect(self.arrayFloat.to16Bits()).to(equal(arrayInt16))
        expect(self.arrayFloat.to32Bits()).to(equal(arrayInt32))
    }
    
    func testOverflow() {
        let overflow: Float = 10
        let overFlowArrayU8: [Float] = [Float(UInt8.max) + overflow, Float(UInt8.max) + overflow]
        let limitU8: [UInt8] = [UInt8.max, UInt8.max]
        expect(overFlowArrayU8.toU8Bits()).to(equal(limitU8))
        
        let overFlowArrayU16: [Float] = [Float(UInt16.max) + overflow, Float(UInt16.max) + overflow]
        let limitU16: [UInt16] = [UInt16.max, UInt16.max]
        expect(overFlowArrayU16.toU16Bits()).to(equal(limitU16))
        
        let overFlowArray8: [Float] = [Float(Int8.max) + overflow, Float(Int8.max) + overflow]
        let limit8: [Int8] = [Int8.max, Int8.max]
        expect(overFlowArray8.to8Bits()).to(equal(limit8))
        
        let overFlowArray16: [Float] = [Float(Int16.max) + overflow, Float(Int16.max) + overflow]
        let limit16: [Int16] = [Int16.max, Int16.max]
        expect(overFlowArray16.to16Bits()).to(equal(limit16))
        
    }
    
    func testPerformance() throws {
        
        let width = 1920
        let height = 1080
        let array = [Float](repeating: 255, count: width * height)
        var result: [UInt8]? = nil
        self.measure() {
            result = array.toU8Bits()
        }
        print(result?.count ?? 0)
    }
}
