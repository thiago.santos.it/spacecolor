//
//  Array+ShiftingTest.swift
//  SpaceColor_Tests
//
//  Created by Thiago Santos on 17/12/22.
//  Copyright © 2022 CocoaPods. All rights reserved.
//

import XCTest
import Nimble
@testable import SpaceColor

final class ArrayShiftingTest: XCTestCase {

    func test() throws {
        let array: [Float] = [1, 2, 3, 4, 5]
        expect(array << 1).to(equal([2, 3, 4, 5, 0]))
        expect(array << 2).to(equal([3, 4, 5, 0, 0]))
        expect(array >> 1).to(equal([0, 1, 2, 3, 4]))
        expect(array >> 2).to(equal([0, 0, 1, 2, 3]))
    }
}
