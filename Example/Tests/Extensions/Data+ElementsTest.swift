//
//  Array+ElementsTest.swift
//  SpaceColor_Tests
//
//  Created by Thiago Santos on 17/12/22.
//  Copyright © 2022 CocoaPods. All rights reserved.
//

import XCTest
import Nimble
@testable import SpaceColor

final class ArrayElementsTest: XCTestCase {

    func test() throws {
        let arrayU8: [UInt8] = [0xFA, 0xFB, 0xFC, 0xFD]
        expect(Data(arrayU8).elements()).to(equal(arrayU8))
        
        let arrayU16: [UInt16] = [0xFBFA, 0xFDFC]
        let result16: [UInt16] = Data(arrayU8).elements()
        expect(result16).to(equal(arrayU16))
        
        let arrayU32: [UInt32] = [0xFDFCFBFA]
        let result32: [UInt32] = Data(arrayU8).elements()
        expect(result32).to(equal(arrayU32))
    }
}
