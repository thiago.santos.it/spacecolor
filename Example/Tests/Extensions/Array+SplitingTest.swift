//
//  Array+SplittingTest.swift
//  SpaceColor_Tests
//
//  Created by Thiago Santos on 17/12/22.
//  Copyright © 2022 CocoaPods. All rights reserved.
//

import Nimble
import XCTest
@testable import SpaceColor

final class ArraySplittingTest: XCTestCase {
    
    func test() throws {
        let line: [Float] = [1.0 ,2.0 ,3.0]
        
        let array = [[Float]](repeating: line, count: 10).flatMap { $0 }
        let splited = array.split(by: line.count)
        let flatten = splited.flatten()

        splited.forEach { row in
            expect(row).to(equal(line))
        }
        
        expect(array).to(equal(flatten))
    }
    
    func testPerformance() throws {
        let width = 1920
        let height = 1080
        let array = [Float](repeating: 0, count: width * height)
        var result: [[Float]]? = nil
        self.measure() {
            result = array.split(by: width)
        }
        print(result?.count ?? 0)
    }
}
