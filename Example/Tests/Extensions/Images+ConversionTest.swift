//
//  Images+ConversionTest.swift
//  SpaceColor_Tests
//
//  Created by Thiago Santos on 17/12/22.
//  Copyright © 2022 CocoaPods. All rights reserved.
//

import XCTest
import Nimble
@testable import SpaceColor

final class ImagesConversionTest: XCTestCase {

    func test() throws {
        guard let imageUrl = Bundle(for: ImagesConversionTest.self).url(forResource: "mona-rgba+bggr", withExtension: "tiff") else {
            XCTFail("Sample image file not found for test, skipping!")
            return
        }
        let nsImage = NSImage(contentsOfFile: imageUrl.path)
        expect(nsImage).toNot(beNil())
        let cgImage = nsImage?.asCGImage()
        expect(cgImage).toNot(beNil())
        let ciImage = nsImage?.asCIImage()
        expect(ciImage).toNot(beNil())
        
        expect(ciImage?.asCGImage()).toNot(beNil())
        expect(ciImage?.asNSImage()).toNot(beNil())
        
        expect(cgImage?.asCIImage()).toNot(beNil())
        expect(cgImage?.asNSImage()).toNot(beNil())
    }
}
