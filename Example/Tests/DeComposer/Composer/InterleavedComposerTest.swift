//
//  InterleavedComposerTest.swift
//  SpaceColor_Tests
//
//  Created by Thiago Santos on 17/12/22.
//  Copyright © 2022 CocoaPods. All rights reserved.
//

import XCTest
import Quick
import Nimble
@testable import SpaceColor

final class InterleavedComposerTest: XCTestCase {
    
    func testThreeComponents() throws {
        let componentA: [Float] = [1, 1, 1]
        let componentB: [Float] = [2, 2, 2]
        let componentC: [Float] = [3, 3, 3]
        let data: [Float] = [1, 2, 3, 1, 2, 3, 1, 2, 3]
        
        let composer = InterleavedComposer(alloc: componentA.count * 3)
        
        let planar = Planar(components1D: [componentA, componentB, componentC], rowSize: 3)
        expect(composer.from(planar: planar).data).to(equal(data))
        expect(composer.from(planar: planar).components).to(equal(3))
    }
    
    func testFourComponents() throws {
        let componentA: [Float] = [1, 1, 1]
        let componentB: [Float] = [2, 2, 2]
        let componentC: [Float] = [3, 3, 3]
        let componentD: [Float] = [4, 4, 4]
        let data: [Float] = [1, 2, 3, 4, 1, 2, 3, 4, 1, 2, 3, 4]
        
        let composer = InterleavedComposer(alloc: componentA.count * 4)

        let planar = Planar(components1D: [componentA, componentB, componentC, componentD], rowSize: 4)
        expect(composer.from(planar: planar).data).to(equal(data))
        expect(composer.from(planar: planar).components).to(equal(4))
    }

    func testFiveComponents() throws {
        let componentA: [Float] = [1, 1, 1]
        let componentB: [Float] = [2, 2, 2]
        let componentC: [Float] = [3, 3, 3]
        let componentD: [Float] = [4, 4, 4]
        let componentE: [Float] = [5, 5, 5]
        let data: [Float] = [1, 2, 3, 4, 5, 1, 2, 3, 4, 5, 1, 2, 3, 4, 5]
        
        let composer = InterleavedComposer(alloc: componentA.count * 5)

        let planar = Planar(components1D: [componentA, componentB, componentC, componentD, componentE], rowSize: 5)
        expect(composer.from(planar: planar).data).to(equal(data))
        expect(composer.from(planar: planar).components).to(equal(5))
    }

    func testPerformance() throws {
        let width = 1920
        let height = 1080
        let numberOfComponents = 4
        let size = width * height
        let componentA: [Float] = [Float](repeating: 1, count: size)
        let componentB: [Float] = [Float](repeating: 2, count: size)
        let componentC: [Float] = [Float](repeating: 3, count: size)
        
        let composer = InterleavedComposer(alloc: componentA.count * 3)

        let planar = Planar(components1D: [componentA, componentB, componentC], rowSize: numberOfComponents * width)

        var result: Interleaved? = nil

        self.measure {
            result = composer.from(planar: planar)
        }
        expect(result).toNot(beNil())
    }
}
