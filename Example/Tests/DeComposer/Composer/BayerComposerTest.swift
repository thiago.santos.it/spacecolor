//
//  BayerComposerTestCase.swift
//  SpaceColor_Tests
//
//  Created by Thiago Santos on 17/12/22.
//  Copyright © 2022 CocoaPods. All rights reserved.
//

import XCTest
import Nimble
@testable import SpaceColor

final class BayerComposerTest: XCTestCase {
  
    func test() throws {
        
        let componentA: [[Float]] = [[0, 1, 0, 1],
                                   [1, 0, 1, 0]]
        
        let componentB: [[Float]] = [[2, 0, 2, 0],
                                   [0, 0, 0, 0]]
        
        let componentC: [[Float]] = [[0, 0, 0, 0],
                                   [0, 3, 0, 3]]
        
        let data: [[Float]] = [[2, 1, 2, 1],
                             [1, 3, 1, 3]]
        
        let planar = Planar(components2D: [componentA, componentB, componentC], rowSize: 4)
        
        let composer = BayerComposer(pattern: .FirstMainMainSecond)
        
        expect(composer.from(planar: planar).data).to(equal(data))
        expect(composer.from(planar: planar).pattern).to(equal(.FirstMainMainSecond))
    }
    

    func testPerformance() throws {
        let width = 1920
        let height = 1080
        let component = [Float](repeating: 1, count: width * height)
       
        let planar = Planar(components1D: [component, component, component], rowSize: width)
        let composer = BayerComposer(pattern: .MainFirstSecondMain)
        var result: Bayer? = nil
        
        self.measure {
            result = composer.from(planar: planar)
        }
        expect(result).toNot(beNil())
    }
}
