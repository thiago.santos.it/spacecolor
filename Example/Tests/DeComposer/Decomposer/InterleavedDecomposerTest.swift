//
//  InterleavedDecomposerTest.swift
//  SpaceColor_Tests
//
//  Created by Thiago Santos on 17/12/22.
//  Copyright © 2022 CocoaPods. All rights reserved.
//

import XCTest
import Quick
import Nimble
import Accelerate
@testable import SpaceColor

final class InterleavedDecomposerTest: XCTestCase {
    
    func testThreeComponents() {
        let line: [Float] = [1, 2, 3,  1, 2, 3]
        let data: [Float] = [[Float]](repeating: line, count: 2).flatten()
        let decomposer = InterleavedDecomposer()
        let planar = decomposer.toPlanarFrom(decomposable: Interleaved(data: data, components: ColorSpace.RGB.components, rowSize: 3))
        
        let decrementA: Float = -1
        let decrementB: Float = -2
        let decrementC: Float = -3
        
        let resultA = vDSP.add(decrementA, planar.components1D[0])
        let resultB = vDSP.add(decrementB, planar.components1D[1])
        let resultC = vDSP.add(decrementC, planar.components1D[2])
        
        expect(vDSP.sum(resultA)).to(equal(Float(0)))
        expect(vDSP.sum(resultB)).to(equal(Float(0)))
        expect(vDSP.sum(resultC)).to(equal(Float(0)))
    }
    
    func testFourComponents() throws {
        let line: [Float] = [1, 2, 3, 4, 1, 2, 3, 4]
        let data: [Float] = [[Float]](repeating: line, count: 2).flatten()
        
        let decomposer = InterleavedDecomposer()
        
        let planar = decomposer.toPlanarFrom(decomposable: Interleaved(data: data, components: ColorSpace.RGBA.components, rowSize: 4))
        
        let decrementA: Float = -1
        let decrementB: Float = -2
        let decrementC: Float = -3
        let decrementD: Float = -4
        
        let resultA = vDSP.add(decrementA, planar.components1D[0])
        let resultB = vDSP.add(decrementB, planar.components1D[1])
        let resultC = vDSP.add(decrementC, planar.components1D[2])
        let resultD = vDSP.add(decrementD, planar.components1D[3])
        
        expect(vDSP.sum(resultA)).to(equal(Float(0)))
        expect(vDSP.sum(resultB)).to(equal(Float(0)))
        expect(vDSP.sum(resultC)).to(equal(Float(0)))
        expect(vDSP.sum(resultD)).to(equal(Float(0)))
    }
     
    func testPerformance() throws {
        let width = 1920
        let height = 1080
        let numberOfComponents = 4
        let rgba: [Float] = [Float](repeating: 1.0, count: width * height * numberOfComponents)
        
        let decomposer = InterleavedDecomposer()
        
        var planar: Planar? = nil
        
        self.measure {
            planar = decomposer.toPlanarFrom(decomposable: Interleaved(data: rgba, components: 4, rowSize: numberOfComponents * width))
        }
        expect(planar).toNot(beNil())
    }

}
