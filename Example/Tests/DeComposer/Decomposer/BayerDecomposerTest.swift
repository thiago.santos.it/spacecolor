//
//  BayerDecomposerTest.swift
//  SpaceColor_Tests
//
//  Created by Thiago Santos on 17/12/22.
//  Copyright © 2022 CocoaPods. All rights reserved.
//

import XCTest
import Nimble
import Quick
@testable import SpaceColor

final class BayerDecomposerTest: XCTestCase {
    
    func testFirstMainMainSecond() throws {
        let matrix: [[Float]] = [[2,1,2,1,2,1,2,1,2,1],
                                 [1,3,1,3,1,3,1,3,1,3],
                                 [2,1,2,1,2,1,2,1,2,1],
                                 [1,3,1,3,1,3,1,3,1,3],
                                 [2,1,2,1,2,1,2,1,2,1],
                                 [1,3,1,3,1,3,1,3,1,3],
                                 [2,1,2,1,2,1,2,1,2,1],
                                 [1,3,1,3,1,3,1,3,1,3],
                                 [2,1,2,1,2,1,2,1,2,1],
                                 [1,3,1,3,1,3,1,3,1,3]]
        
        let finalA: [[Float]] = [[0,1,0,1,0,1,0,1,0,1],
                                 [1,0,1,0,1,0,1,0,1,0],
                                 [0,1,0,1,0,1,0,1,0,1],
                                 [1,0,1,0,1,0,1,0,1,0],
                                 [0,1,0,1,0,1,0,1,0,1],
                                 [1,0,1,0,1,0,1,0,1,0],
                                 [0,1,0,1,0,1,0,1,0,1],
                                 [1,0,1,0,1,0,1,0,1,0],
                                 [0,1,0,1,0,1,0,1,0,1],
                                 [1,0,1,0,1,0,1,0,1,0]]
        
        let finalB: [[Float]] = [[2,0,2,0,2,0,2,0,2,0],
                                 [0,0,0,0,0,0,0,0,0,0],
                                 [2,0,2,0,2,0,2,0,2,0],
                                 [0,0,0,0,0,0,0,0,0,0],
                                 [2,0,2,0,2,0,2,0,2,0],
                                 [0,0,0,0,0,0,0,0,0,0],
                                 [2,0,2,0,2,0,2,0,2,0],
                                 [0,0,0,0,0,0,0,0,0,0],
                                 [2,0,2,0,2,0,2,0,2,0],
                                 [0,0,0,0,0,0,0,0,0,0]]
        
        let finalC: [[Float]] = [[0,0,0,0,0,0,0,0,0,0],
                                 [0,3,0,3,0,3,0,3,0,3],
                                 [0,0,0,0,0,0,0,0,0,0],
                                 [0,3,0,3,0,3,0,3,0,3],
                                 [0,0,0,0,0,0,0,0,0,0],
                                 [0,3,0,3,0,3,0,3,0,3],
                                 [0,0,0,0,0,0,0,0,0,0],
                                 [0,3,0,3,0,3,0,3,0,3],
                                 [0,0,0,0,0,0,0,0,0,0],
                                 [0,3,0,3,0,3,0,3,0,3]]
        
        let decomposer = BayerDecomposer(width: matrix[0].count, height: matrix.count)
        
        let planar = decomposer.toPlanarFrom(decomposable: Bayer(data: matrix, pattern: .FirstMainMainSecond, rowSize: matrix[0].count))
        
        expect(planar.components2D[0]).to(equal(finalA))
        expect(planar.components2D[1]).to(equal(finalB))
        expect(planar.components2D[2]).to(equal(finalC))
    }
    
    func testMainFirstSecondMain() throws {
        let matrix: [[Float]] = [[1,2,1,2,1,2,1,2,1,2],
                                 [3,1,3,1,3,1,3,1,3,1],
                                 [1,2,1,2,1,2,1,2,1,2],
                                 [3,1,3,1,3,1,3,1,3,1],
                                 [1,2,1,2,1,2,1,2,1,2],
                                 [3,1,3,1,3,1,3,1,3,1],
                                 [1,2,1,2,1,2,1,2,1,2],
                                 [3,1,3,1,3,1,3,1,3,1],
                                 [1,2,1,2,1,2,1,2,1,2],
                                 [3,1,3,1,3,1,3,1,3,1]]
        
        let finalA: [[Float]] = [[1,0,1,0,1,0,1,0,1,0],
                                 [0,1,0,1,0,1,0,1,0,1],
                                 [1,0,1,0,1,0,1,0,1,0],
                                 [0,1,0,1,0,1,0,1,0,1],
                                 [1,0,1,0,1,0,1,0,1,0],
                                 [0,1,0,1,0,1,0,1,0,1],
                                 [1,0,1,0,1,0,1,0,1,0],
                                 [0,1,0,1,0,1,0,1,0,1],
                                 [1,0,1,0,1,0,1,0,1,0],
                                 [0,1,0,1,0,1,0,1,0,1]]
        
        let finalB: [[Float]] = [[0,2,0,2,0,2,0,2,0,2],
                                 [0,0,0,0,0,0,0,0,0,0],
                                 [0,2,0,2,0,2,0,2,0,2],
                                 [0,0,0,0,0,0,0,0,0,0],
                                 [0,2,0,2,0,2,0,2,0,2],
                                 [0,0,0,0,0,0,0,0,0,0],
                                 [0,2,0,2,0,2,0,2,0,2],
                                 [0,0,0,0,0,0,0,0,0,0],
                                 [0,2,0,2,0,2,0,2,0,2],
                                 [0,0,0,0,0,0,0,0,0,0]]
        
        let finalC: [[Float]] = [[0,0,0,0,0,0,0,0,0,0],
                                 [3,0,3,0,3,0,3,0,3,0],
                                 [0,0,0,0,0,0,0,0,0,0],
                                 [3,0,3,0,3,0,3,0,3,0],
                                 [0,0,0,0,0,0,0,0,0,0],
                                 [3,0,3,0,3,0,3,0,3,0],
                                 [0,0,0,0,0,0,0,0,0,0],
                                 [3,0,3,0,3,0,3,0,3,0],
                                 [0,0,0,0,0,0,0,0,0,0],
                                 [3,0,3,0,3,0,3,0,3,0]]
        
        let decomposer = BayerDecomposer(width: matrix[0].count, height: matrix.count)
        let planar = decomposer.toPlanarFrom(decomposable: Bayer(data: matrix, pattern: BayerPattern.MainFirstSecondMain, rowSize: matrix[0].count))
        
        expect(planar.components2D[0]).to(equal(finalA))
        expect(planar.components2D[1]).to(equal(finalB))
        expect(planar.components2D[2]).to(equal(finalC))
    }
    
    func testPerformanceExample() throws {
        let width = 1920
        let height = 1080
        let numberOfComponents = 1

        let data = [Float](repeating: 1, count: width * height * numberOfComponents).split(by: width)

        var planar: Planar? = nil
        let decomposer = BayerDecomposer(width: width, height: height)

        self.measure {
            planar = decomposer.toPlanarFrom(decomposable: Bayer(data: data, pattern: .MainFirstSecondMain, rowSize: width))
        }
        expect(planar).toNot(beNil())

    }

}
