//
//  PlanarTest.swift
//  SpaceColor_Tests
//
//  Created by Thiago Santos on 17/12/22.
//  Copyright © 2022 CocoaPods. All rights reserved.
//

import XCTest
import Nimble
@testable import SpaceColor

final class PlanarTest: XCTestCase {

    func test1Dto2D() throws {
        let width = 8
        let height = 8
        let size = width * height

        let dataCA = [Float](repeating: 1, count: size)
        let dataCB = [Float](repeating: 2, count: size)
        let dataCC = [Float](repeating: 3, count: size)

        let planar = Planar(components1D: [dataCA, dataCB, dataCC], rowSize: width)

        expect(planar.components2D[0].flatten()).to(equal(dataCA))
        expect(planar.components2D[1].flatten()).to(equal(dataCB))
        expect(planar.components2D[2].flatten()).to(equal(dataCC))
    }
    
    func test2Dto1D() throws {
        let width = 8
        let height = 8
        let size = width * height

        let dataCA = [Float](repeating: 1, count: size).split(by: width)
        let dataCB = [Float](repeating: 2, count: size).split(by: width)
        let dataCC = [Float](repeating: 3, count: size).split(by: width)

        let planar = Planar(components2D: [dataCA, dataCB, dataCC], rowSize: width)

        expect(planar.components1D[0]).to(equal(dataCA.flatten()))
        expect(planar.components1D[1]).to(equal(dataCB.flatten()))
        expect(planar.components1D[2]).to(equal(dataCC.flatten()))
    }
}
