#  Things to do!

- ColorModel finalizar cores faltantes 
- Create BayerPixelPattern convesions
- Tests for Transformation Package
- Create real tests for interpolation
- Write HQLinearInterpolation
- Write docs with next steps (New interpolations, more tests and profile lab color transformations)
- Write component interface

```
high: [pu:Image] +- [pu:Transformation] -+ [pr:ColorModel]
                                     \---+ [pr:Pixel]
                                      \--+ [pr:Channel]
----------------------------------------
Low: [pu:Debayering] [pr:DeComposer]
```      

- Publish
